let express = require('express');
let app = express();
let PF = require('pathfinding');
let path = require('path');
let cors = require('cors')
let Utils = require('./js/utils.js');
let WorldServer = require('./js/worldserver.js');

//app.options('*', cors()) // include before other routes

app.use(cors({
	origin: 'http://localhost:5173',
	credentials: true,
}));

app.all('*', function (req, res) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Content-Type,Content-Length, Authorization, Accept,X-Requested-With");
	res.header("Access-Control-Allow-Methods", "PUT,POST,GET,DELETE,OPTIONS");
	//...
});

// allow media
app.use(express.static( __dirname + '/../client') );
app.use(express.static( __dirname + '/../sprites') );
app.use(express.static( __dirname + '/../shared') );

const http = require('http').Server(app);
//const io = require('socket.io')(http);
const io = require('socket.io')(http, {
	'pingInterval': 2000,
	'pingTimeout': 10000,
	cors: {
		origin: '*',
	}
});
//io.set('origins', '*')
//io.set('transports', ['websocket']);

app.get('/', (req, res) => {
	res.sendFile( path.join(__dirname, '/../client/index.html') );
});

let myWorldserver = new WorldServer(io);

http.listen(3000, () => {
	console.log('listening on *:3000');
});