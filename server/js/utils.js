//let sanitizer = require('sanitizer');
let Types = require('./gametypes.js');
let Utils = {};

module.exports = Utils;

Utils.sanitize = function (string)
{
    // Strip unsafe tags, then escape as html entities.
    return sanitizer.escape(sanitizer.sanitize(string));
};

Utils.time = (unit) =>
{
    const hrTime = process.hrtime();

    switch (unit) {

        case 'milli':
            return hrTime[0] * 1000 + hrTime[1] / ( 1000 * 1000 );

        case 'micro':
            return hrTime[0] * 1000000 + hrTime[1] / 1000;

        case 'nano':
        default:
            return hrTime[0] * 1000000000 + hrTime[1];
    }
};

Utils.random = function (range)
{
    return Math.floor(Math.random() * range);
};

Utils.randomRange = function (min, max)
{
    return min + (Math.random() * (max - min));
};

Utils.randomInt = function (min, max)
{
    return min + Math.floor(Math.random() * (max - min + 1));
};

Utils.clamp = function (min, max, value)
{
    if (value < min) {
        return min;
    } else if (value > max) {
        return max;
    } else {
        return value;
    }
};

Utils.randomOrientation = function ()
{
    let o, r = Utils.random(4);

    if (r === 0) {
        o = Types.Orientations.LEFT;
    }
    if (r === 1) {
        o = Types.Orientations.RIGHT;
    }
    if (r === 2) {
        o = Types.Orientations.UP;
    }
    if (r === 3) {
        o = Types.Orientations.DOWN;
    }

    return o;
};

Utils.Mixin = function (target, source)
{
    if (source) {
        for (let key, keys = Object.keys(source), l = keys.length; l--;) {
            key = keys[l];

            if (source.hasOwnProperty(key)) {
                target[key] = source[key];
            }
        }
    }
    return target;
};

Utils.getRange = function(y1,x1,y2,x2)
{
    let a = x1 - x2;
    let b = y1 - y2;

    let c = Math.sqrt( a*a + b*b );

    c = Math.ceil(c);

    return c;
};

// Converts numeric degrees to radians
Utils.toRad = function (Value)
{
    return Value * Math.PI / 180;
}

Utils.distanceTo = function (x, y, x2, y2)
{
    let distX = Math.abs(x - x2),
        distY = Math.abs(y - y2);

    return (distX > distY) ? distX : distY;
};

Utils.NaN2Zero = function(num)
{
    if(isNaN(num*1)){
        return 0;
    } else{
        return num*1;
    }
};

Utils.trueFalse = function(bool)
{
    return bool === "true" ? true : false;
};

Utils.uuidv4 = function()
{
	return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
		var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
		return v.toString(16);
	});
};

Utils.randomString = function(length, chars)
{
	var result = '';

	for (var i = length; i > 0; --i)
	    result += chars[Math.floor(Math.random() * chars.length)];

	return result;
};

Utils.coordQuad = function(coord, factor)
{
    if( !factor )
        factor = 2;

    let y = Math.floor(coord[0] / factor);
    let x = Math.floor(coord[1] / factor);

    //console.log("quadration");
    //console.log(coord);
    //console.log([y,x]);

    return [y,x];
};
