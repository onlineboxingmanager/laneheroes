<?php
#ä

ini_set('allow_url_fopen ', true);

function ripSprite($myParams, $file)
{
	$url = 'https://ro-character-simulator.ratemyserver.net/charsim.php?';
	$uri = array();
	foreach( $myParams as $key => $value )
		$uri[] = $key . '=' . $value;
	$url = $url . implode('&', $uri);

	echo $url."\n";

	$curlSession = curl_init();
	curl_setopt($curlSession, CURLOPT_URL, $url);
	curl_setopt($curlSession, CURLOPT_BINARYTRANSFER, true);
	curl_setopt($curlSession, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curlSession, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($curlSession, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($curlSession, CURLOPT_SSL_VERIFYPEER, 0);

	$image = curl_exec($curlSession);

	curl_close($curlSession);

	file_put_contents($file, $image);
}

$params = array();
$params['gender'] = '0';
$params['job'] = '0';
$params['hair'] = '1';
$params['viewid'] = '';
$params['location'] = '';
$params['direction'] = '';
$params['action'] = '';
$params['hdye'] = '';
$params['dye'] = '';

$params['framenum'] = '';
$params['bg'] = '';
$params['cart'] = '';
$params['mount'] = '';
$params['shield'] = '';
$params['weapon'] = '';
$params['rand'] = rand(10000, 999999);

$config = array();
$config['gender'] = array(0 => 'female', 1 => 'male');
$config['job'] = array
(

	0 => 'novice', // newbie
	1 => 'sword', // sword
	2 => 'mage', // mage
	3 => 'archer', // archer
	4 => 'acolyte', // acolyte
	5 => 'merch', // merch
	6 => 'thief', // thief

	7 => 'knight', // knight
	8 => 'priest', // priest
	9 => 'wizard', // wiz
	10 => 'blacksmith', // blacksmith
	11 => 'hunter', // hunter
	12 => 'assassin', // assa

	14 => 'paladin', // paladin
	15 => 'monk', // monk
	16 => 'sage', // sage
	17 => 'rogue', // rouge
	18 => 'alchemist', // ?? alchemist ?
	19 => 'gypsi', // dancer ?
	20 => 'dancer', // dancer

	// special
	13 => '', // mounted knigth
	21 => '', // mounted pala

	4022 => 'king',
	5006 => 'king2',
	5014 => 'king3',

	5000 => 'rune_knight'
);

$chars = [];

// http://ro-character-simulator.ratemyserver.net/charsim.php?gender=1&job=1&hair=28&viewid=&location=&direction=0&action=10&hdye=0&dye=0&framenum=0&bg=0&cart=&mount=0&shield=0&weapon=1154&rand=516537
$chars[] =
[
	'gender' => 1,
	'job' => 1,
	'weapon' => 1154,
	'hair' => 28,
	'shield' => 3
];

// http://ro-character-simulator.ratemyserver.net/charsim.php?gender=1&job=8&hair=2&viewid=&location=&direction=0&action=4&hdye=3&dye=0&framenum=0&bg=0&cart=&mount=0&shield=3&weapon=1558&rand=3542697
$chars[] =
[
	'gender' => 1,
	'job' => 8,
	'weapon' => 1558,
	'hair' => 2,
];

// http://ro-character-simulator.ratemyserver.net/charsim.php?gender=0&job=11&hair=33&viewid=19&location=256&direction=0&action=11&hdye=0&dye=0&framenum=0&bg=0&cart=&mount=0&shield=3&weapon=1720&rand=1784169
$chars[] =
[
	'gender' => 0,
	'job' => 11,
	'weapon' => 1720,
	'hair' => 33,
	'viewid' => 19,
];

// http://ro-character-simulator.ratemyserver.net/charsim.php?gender=1&job=9&hair=7&viewid=&location=&direction=0&action=4&hdye=0&dye=0&framenum=0&bg=0&cart=&mount=0&shield=0&weapon=1613&rand=2951466
$chars[] =
[
	'gender' => 1,
	'job' => 9,
	'weapon' => 1613,
	'hair' => 7,
];

// http://ro-character-simulator.ratemyserver.net/charsim.php?gender=0&job=5000&hair=29&viewid=&location=&direction=0&action=4&hdye=0&dye=0&framenum=0&bg=0&cart=&mount=0&shield=2&weapon=1126&rand=1498135
$chars[] =
[
	'gender' => 0,
	'job' => 5000,
	'weapon' => 1126,
	'hair' => 29,
	'viewid' => 380,
	'shield' => 2
];

// http://ro-character-simulator.ratemyserver.net/charsim.php?gender=0&job=20&hair=46&viewid=&location=256&direction=0&action=11&hdye=0&dye=0&framenum=0&bg=0&cart=&mount=0&shield=0&weapon=1710&rand=8669987
$chars[] =
[
	'gender' => 0,
	'job' => 20,
	'weapon' => 1710,
	'hair' => 46,
];

// http://ro-character-simulator.ratemyserver.net/charsim.php?gender=1&job=14&hair=26&viewid=&location=&direction=0&action=4&hdye=0&dye=0&framenum=1&bg=0&cart=&mount=0&shield=4&weapon=1129&rand=3736707
$chars[] =
[
	'gender' => 1,
	'job' => 14,
	'weapon' => 1129,
	'shield' => 4,
	'hair' => 26,
];

$chars[] =
	[
		'gender' => 1,
		'job' => 0,
		'weapon' => 1220,
		'hair' => 25,
	];

//https://ro-character-simulator.ratemyserver.net/charsim.php?gender=1&job=3&hair=7&viewid=&location=&direction=0&action=4&hdye=0&dye=0&framenum=0&bg=0&cart=&mount=0&shield=0&weapon=1713&rand=3037963
$chars[] =
	[
		'gender' => 1,
		'job' => 3,
		'weapon' => 1713,
		'hair' => 7,
	];

//https://ro-character-simulator.ratemyserver.net/charsim.php?gender=1&job=2&hair=7&viewid=&location=&direction=0&action=4&hdye=0&dye=0&framenum=0&bg=0&cart=&mount=0&shield=0&weapon=0&rand=6838503
$chars[] =
	[
		'gender' => 1,
		'job' => 2,
		'hair' => 7,
	];

// https://ro-character-simulator.ratemyserver.net/charsim.php?gender=1&job=6&hair=7&viewid=&location=&direction=0&action=4&hdye=0&dye=0&framenum=0&bg=0&cart=&mount=0&shield=0&weapon=1216&rand=3895197
$chars[] =
	[
		'gender' => 1,
		'job' => 6,
		'hair' => 7,
		'weapon' => 1216,
	];

// https://ro-character-simulator.ratemyserver.net/charsim.php?gender=1&job=17&hair=7&viewid=&location=&direction=0&action=4&hdye=0&dye=0&framenum=0&bg=0&cart=&mount=0&shield=0&weapon=1216&rand=2740436
$chars[] =
	[
		'gender' => 1,
		'job' => 17,
		'hair' => 7,
		'weapon' => 1216,
	];

// https://ro-character-simulator.ratemyserver.net/charsim.php?gender=1&job=4022&hair=1&viewid=&location=0&direction=0&action=4&hdye=0&dye=0&framenum=0&bg=0&cart=&mount=0&shield=0&weapon=1461&rand=8995896
$chars[] =
	[
		'gender' => 1,
		'job' => 4022,
		'hair' => 1,
		'weapon' => 1461,
		'viewid' => 45
	];

// https://ro-character-simulator.ratemyserver.net/charsim.php?gender=1&job=5006&hair=7&viewid=45&location=256&direction=5&action=0&hdye=0&dye=0&framenum=8&bg=0&cart=&mount=0&shield=0&weapon=1457&rand=9581554
$chars[] =
	[
		'gender' => 1,
		'job' => 5006,
		'hair' => 7,
		'weapon' => 1457,
		'viewid' => 45
	];

// https://ro-character-simulator.ratemyserver.net/charsim.php?gender=1&job=5014&hair=25&viewid=45&location=256&direction=0&action=4&hdye=0&dye=0&framenum=0&bg=0&cart=&mount=0&shield=0&weapon=1458&rand=996582
$chars[] =
	[
		'gender' => 1,
		'job' => 5014,
		'hair' => 25,
		'weapon' => 1458,
		'viewid' => 45
	];

// all directions
$config['direction'] = array(0, 1, 2, 3, 4, 5, 6, 7);
$config['action'] = array
(
	0 => array( 0 ), // stehen
	1 => array( 0, 1, 2, 3, 4, 5, 6, 7 ), // laufen
	2 => array( 0 ), // sitzen
	3 => array( 0, 1, 2 ), // aufheben
	4 => array( 0, 1, 2, 3, 4, 5 ), // kampfhaltung 1
	5 => array( 0, 1, 2, 3, 4 ), // angriff
	6 => array( 0, 1, 2 ), // selbst getroffen
	7 => array( 0 ), // sterben
	8 => array( 0 ), // tot
	9 => array( 0 ), // alt attack 1
	10 => array( 0, 1, 2, 3, 4, 5, 6, 7, 8 ), // alt attack 2
	11 => array( 0, 1, 2, 3, 4, 5, 6, 7 ), // alt attack 3
	12 => array( 0, 1, 2, 3, 4, 5 ), // zaubern
);

// generiere sprites
$base_path = __DIR__ . '/../sprites';
foreach( $chars as $char )
{
	$gendername = $config['gender'][$char['gender']];
	$jobname = $config['job'][$char['job']];

	foreach( $config['direction'] as $direction )
	{
		foreach( $config['action'] as $action_key => $framelist )
		{
			foreach( $framelist as $frame )
			{

				//$dir = $base_path . '/' . $gendername . '/' . $jobname;
				$dir = $base_path . '/character/' . $jobname;
				if( !is_dir($dir) )
					mkdir($dir, 0755, true);

				$myParams = $params;
				$myParams['gender'] = $char['gender'];
				$myParams['job'] = $char['job'];
				$myParams['hair'] = $char['hair'];

				$myParams['direction'] = $direction;
				$myParams['action'] = $action_key;
				$myParams['location'] = 256;

				$myParams['framenum'] = $frame;
				$myParams['rand'] = rand(10000, 999999);

				$myParams['viewid'] = @$char['viewid'];
				$myParams['shield'] = @$char['shield'];
				$myParams['weapon'] = @$char['weapon'];

				$file = $dir . '/direction_'.$direction.'__action_'.$action_key.'__frame_'.$frame.'.png';

				if( file_exists($file) AND filesize($file) > 0 )
					continue;

				ripSprite($myParams, $file);
			}
		}
	}

}



/*

$config['job'] = array(0, 1, 2, 3);

// jeden job
$base_path = __DIR__ . '/rips';
foreach( $config['gender'] as $gender => $gendername )
{
	foreach( $config['job'] as $job )
	{
		foreach( $config['direction'] as $direction )
		{
			foreach( $config['action'] as $action_key => $framelist )
			{
				foreach( $framelist as $frame )
				{

					$dir = $base_path . '/' . $gender . '/' . $job . '/' . $params['hair'];
					if( !is_dir($dir) )
						mkdir($dir, 0755, true);

					$myParams = $params;
					$myParams['gender'] = $gender;
					$myParams['job'] = $job;
					$myParams['direction'] = $direction;
					$myParams['action'] = $action_key;
					$myParams['framenum'] = $frame;
					$myParams['rand'] = rand(10000, 999999);

					$file = $dir . '/direction_'.$direction.'__action_'.$action_key.'__frame_'.$frame.'.png';

					if( file_exists($file) AND filesize($file) > 0 )
						continue;

					ripSprite($myParams, $file);
				}
			}
		}
	}
}