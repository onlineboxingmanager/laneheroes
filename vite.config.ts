import { defineConfig } from 'vite'
import { svelte } from '@sveltejs/vite-plugin-svelte'
import mkcert from 'vite-plugin-mkcert'
import {resolve} from "path";

// https://vitejs.dev/config/
export default defineConfig({
  server: {
    cors: false
  },
  plugins: [
    {
      name: 'html',
      handleHotUpdate({file, server}) {
        if (file.endsWith('.svelte') || file.endsWith('.html')) {
          server.ws.send({
            type: 'full-reload',
            path: '*'
          });
        }
      }
    },
    //mkcert(),
    svelte({
      //preprocess: [optimizeImports()],
    }),
  ],

})
