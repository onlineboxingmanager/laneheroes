let BaseClass = require('./lib/class');
let Message = require('./message');
let Utils = require('./utils');
let PF = require('pathfinding');

class Entity extends BaseClass
{

    constructor(id, type, kind, position)
    {
    	super();

    	this.game = null;
        this.id = parseInt(id, 10);
        this.name;
        this.uid = Utils.uuidv4();
        this.type = type;
        this.kind = kind;
        this.position = position;
	    this.pos_org = [].concat(position);
        this.direction = 's';

        this.timer = null;

        this.team;
        this.player;

	    this.path = [];
	    this.speed = 0;
	    this.attack_speed = 0;
	    this.life = 1000;
	    this.maxlife = 1000;
	    this.is_flying = 0;
	    this.damage = [0, 0];
	    this.armor = 0;
	    this.evasion = 0;
	    this.range = 1;

		this.is_moving = false;
	    this.skills = {};
		this.target = [];

	    this.cost = 0; // kosten
	    this.money = 0; // belohnung

	    this.game = null;
	    this.last_action = process.hrtime.bigint();

	    this.finder = new PF.AStarFinder({
		    allowDiagonal: true,
		    dontCrossCorners: true
	    });
    };

    destroy()
    {

    };

	_getBaseState()
    {
        return {
	        uid: this.uid,
	        id: parseInt(this.id, 10),
	        n: this.name,
	        t: this.type,
	        p: this.position,
	        d: this.direction,
	        l: this.life,
	        m: this.maxlife,
	        s: this.speed,
	        pl: this.player.playerid,
	        ta: this.target.uid
        };
    };

	getState()
    {
        return this._getBaseState();
    };

	spawn()
    {
    	this.direction = 's';
        return Message.spawn(this);
    };

	despawn()
    {
        return Message.despawn(this);
    };

	/**
	 * TODO: die Funktion darf nur einen Schritt machen -> danach muss der Pfad wieder geprüft werden ... -> hohe performance -> aber nötig wenn monster und defender sich gleichzeitig bewegen
	 * theoretisch kann nach einem Schritt die Zielposition bei allen anderen objecten als Pfad entfernt werden. macht das sinn statt auf jeden move ein pfadprüfung zu machen?
	 * @param paths
	 * @param duration
	 * @param callback
	 */
	move(paths, duration, callback)
    {
	    let self = this;

    	clearTimeout(this.timer);
        this.timer = setTimeout(function()
        {
            let path = paths.shift(); // remove first item from list

	        //console.log("move to " + path);

			// get directions (TODO: könnte auch der client machen ? Serverseitig nur Sinn wenn Backstabs usw. eingebaut werden. oder für Casts in bestimmte Richtung)
	        if( paths.length > 0 )
	            self.setPosition(path);

            if( callback && paths.length === 0 )
            {
                return callback();
            }

            // move char on client
            self.game.sendSockets('move entity', self.getState()); // remove me - replaced by server ticks

            if( paths.length === 0 )
            {
	            self.game.sendSockets('stop entity', self.getState());
                console.log("stop entity at " + path[0] + ", " + path[1]);
                return false;
            }

            // start next
            return self.move(paths, self.speed, callback);

        }, duration);
    };

	walkTo(target, callback)
	{
		let self = this;

		this.target = target;

		// Entfernung zum ziel
		let distance = Utils.distanceTo(this.position[0], this.position[1], target.position[0], target.position[1]);

		//console.log("distance " + distance + ' my radius: ' + this.range);
		if( this.life === 0 )
			return this.doDead();

		// attack next enemy if target is already dead
		if( target.life === 0 )
			return this.attackNearby()

		// check skills (damage)
		for( let skillid in this.skills )
		{
			if( this.skills[skillid].is_cd === true || this.skills[skillid].type !== 'damage' )
				continue;

			// check range
			if( this.skills[skillid].range >= distance )
			{
				return this.useSkill(this.skills[skillid], target, function()
				{
					self.attackNearby();
				});
			}
		}

		// check heal spells
		for( let skillid in this.skills )
		{
			if( this.skills[skillid].is_cd === true || this.skills[skillid].type !== 'heal' )
				continue;

			// check range
			for( let i in this.player.chars )
			{
				if( this.player.chars[i].life === this.player.chars[i].maxlife || this.player.chars[i].life <= 0 )
					continue;

				let distance_heal = Utils.distanceTo(this.position[0], this.position[1], this.player.chars[i].position[0], this.player.chars[i].position[1]);
				if( distance <= this.range )
					return this.useSkill(this.skills[skillid], this.player.chars[i], function()
					{
						self.attackNearby();
					});
			}

		}

		// Normaler Angriff
		//  - ist ziel schon im angriffs radius ?
		if( distance <= this.range )
			if( callback )
				return callback();
			else
				return false;

		//console.log("move to " + path);

		const path = this.findPath(target)

		// get directions (TODO: könnte auch der client machen ? Serverseitig nur Sinn wenn Backstabs usw. eingebaut werden. oder für Casts in bestimmte Richtung)
		if( path )
			this.setPosition(path);

		// start moving
		this.is_moving = true;

		// move char on client
		this.game.sendSockets('move entity', this.getState()); // remove me - replaced by server ticks

		clearTimeout(this.timer);
		this.timer = setTimeout(function()
		{
			self.walkTo(target, callback);
		}, this.speed);

	}

	findPath(target)
	{
		// orginal grid laden
		let gridBackup = this.game.grid.clone();

		// remove waypoints
		for( let x in this.game.players )
		{
			for( let i in this.game.players[x].monsters )
			{
				let entity = this.game.players[x].monsters[i]
				let position = entity.position;

				// skip selected target
				if( position.toString() === target.position.toString() )
					continue;

				// skip dead bodies or entity in moving state
				if( entity.life <= 0 || entity.is_moving )
					continue

				// other points are not walkable
				gridBackup.setWalkableAt(position[0], position[1], false);
				console.log("non walkable at " + position.toString());
			}
		}

		// wegfindung
		let finder = new PF.AStarFinder({
			allowDiagonal: true,
			dontCrossCorners: true
		})

		// wegfindung mit aktueller position des ziels (achtung ziel bewegt sich auch)
		let paths = finder.findPath(this.position[0], this.position[1], target.position[0], target.position[1], gridBackup);

		paths.shift(); // start position entfernen
		paths.pop(); // ziel position entfernen

		// ersten Wegpunkt laufen - danach wieder "walkTo" triggern
		return paths.shift(); // remove first item from list
	}

	attackNearby()
	{

	}

	attack(target, callback)
	{
		let self = this;
		clearTimeout(self.timer);

		if( this.life <= 0 )
			return this.doDead();

		// stop moving
		this.is_moving = false;

		let damage = Utils.randomRange(this.damage[0], this.damage[1]);
		damage = parseInt(Math.ceil(damage)) - (target.armor * 5);

		this.setDirection(target.position);

		//console.log("damage calc " + this.damage + " -> " + damage);
		//console.log("target " + target);

		//damage -= target.armor;
		let rand = Utils.randomInt(1, 100);
		let is_hit = ( rand > target.evasion );

		// reduce life
		if( is_hit )
			target.life -= damage;

		//console.log("damage " + damage + " / life left: " + target.life);

		// attack animation
		this.game.sendSockets('attack entity', self.getState());

		// target got hittet
		if( is_hit )
		{
			let obj = target.getState();
			obj.dmg = damage;
			this.game.sendSockets('hit entity', obj);
		}

		// target miss
		if( !is_hit )
			this.game.sendSockets('miss entity', this.getState());

		// target dead
		if ( target.life <= 0 )
		{
			clearTimeout(target.timer);
			target.timer = setTimeout(function()
			{
				return target.doDead();
			}, 1000); // send info after hit message

			if( callback )
				return callback(); // stop here

			return false;
		}

		// next attack if target is alive
		self.timer = setTimeout(function()
		{
			self.attack(target, callback);
		}, self.attack_speed);
 	};

	doDead()
	{
		this.life = 0;

		// stop moving
		this.is_moving = false;

		this.game.sendSockets('dead entity', this.getState());
		//console.log("dead");

		this.player.money += this.money;
		this.game.sendSockets('player status', this.player.getState());

		// alle timeouts stoppen
		clearTimeout( this.timer );

		if( this.type === 'emperium' )
		{
			this.game.sendSockets('game over');
		}

		if( this.type === 'monster' )
			delete this;
	};

	useSkill(skill, target, callback)
	{
		let self = this;

		// stop moving
		this.is_moving = false;

		// in "range" hier nicht mehr prüfen ...
		console.log("use skill - search for monster in skill-range");

		let damage = Utils.randomRange(skill.damage[0], skill.damage[1]);
		damage = parseInt( Math.ceil(damage) );

		// schaden machen auf position + radius
		// TODO: monster in enemies umbenennen - dann können auch monster skills einsetzen
		let hits = [];

		if( skill.type === 'damage' ) // AoE Damage !!
		{
			for( let i in this.player.monsters )
			{
				let monster = this.player.monsters[i];
				if( monster.life <= 0 )
					continue;

				let is_range_y = ( monster.position[0] <= ( target.position[0] + skill.radius ) && monster.position[0] >= ( target.position[0] - skill.radius ) );
				let is_range_x = ( monster.position[1] <= ( target.position[1] + skill.radius ) && monster.position[1] >= ( target.position[1] - skill.radius ) );
				let is_range = ( is_range_x && is_range_y );

				is_range = true;
				if( is_range )
				{
					console.log("monster is in range ... do skill now .....");

					monster.life -= damage;

					let obj = monster.getState();
					obj.dmg = damage;

					this.game.sendSockets('hit entity', obj);
					this.game.sendSockets('skill use', [skill.name, target.position]);

					// target dead
					if ( monster.life <= 0 )
					{
						clearTimeout(monster.timer);
						monster.timer = setTimeout(function()
						{
							monster.doDead();
						}, 1000); // send info after hit message
					}

					hits.push(monster);
				}
			}
		}

		if( skill.type === 'heal' ) // Singe Target Heal TODO
		{
			// start cast
			let obj = this.getState();
			obj.casttime = skill.casttime;
			self.game.sendSockets('cast', obj);

			clearTimeout(this.timer);
			this.timer = setTimeout(function()
			{
				target.life += damage;
				if( target.life > target.maxlife )
					target.life = target.maxlife;

				let obj = target.getState();
				obj.dmg = damage;

				self.game.sendSockets('heal entity', obj);
				self.game.sendSockets('skill use', [skill.name, target.position, self.getState()]);

				skill.is_cd = true;
				setTimeout(function()
				{
					skill.is_cd = false;
				}, skill.cooldown );

				return callback();

			}, skill.casttime );

			return;
		}

		// TODO: cast time !!!
		if( hits.length === 0 )
			return callback();

		skill.is_cd = true;
		setTimeout(function()
		{
			skill.is_cd = false;
		}, skill.cooldown + skill.casttime );

		return callback();
	};

	reset()
	{
		this.life = this.maxlife;
		this.position = this.pos_org;
		this.direction = 's';
	};

	setPosition(pos)
	{
		this.setDirection(pos);
		this.position = pos;
	};

	setDirection(pos)
	{
		let direction = 's';

		let y = pos[0];
		let x = pos[1];

		if( x > this.position[1] && y === this.position[0] )
			direction = 'o';
		if( x < this.position[1] && y === this.position[0] )
			direction = 'w';
		if( y > this.position[0] && x === this.position[1] )
			direction = 's';
		if( y < this.position[0] && x === this.position[1] )
			direction = 'n';

		if( y < this.position[0] && x < this.position[1] )
			direction = 'nw';
		if( y < this.position[0] && x > this.position[1] )
			direction = 'no';
		if( y > this.position[0] && x < this.position[1] )
			direction = 'sw';
		if( y > this.position[0] && x > this.position[1] )
			direction = 'so';

		this.direction = direction;
	};

	stopTimer()
	{
		clearTimeout( this.timer );
	};

}

module.exports = Entity;