<?php
#�

$base_path = __DIR__ . '/../sprites/effects/';

$config = array();

$tile = 240;
$min_size = 1024;
$tileset_height = $tile * 1;

$css_mob_base = '';

foreach (glob($base_path . "/*", GLOB_ONLYDIR) as $folder )
{

	$effectname = basename($folder);
	var_dump($effectname);

	$files = glob( $folder . '/*.png' );
	$tileset_width = $tile * count($files);

	$full_tileset = imagecreatetruecolor( $tileset_width, $tileset_height );

	$full_tileset_color = imagecolorallocatealpha($full_tileset, 0, 0, 0, 127);
	imagefill($full_tileset,0,0, $full_tileset_color);

	imagealphablending($full_tileset, false);
	imagesavealpha($full_tileset, true);

	$filled = [];

	foreach( $files as $i => $file )
	{
		$y = 0;
		$x = $i; // singlerow

		$size = filesize($file);
		$is_empty = ( $size <= $min_size );

		$filled[$y][$x] = $size;

		if( $is_empty )
			continue;

		$image = imagecreatetruecolor( $tile, $tile );

		$transparent = imagecolorallocatealpha($image, 0, 0, 0, 127);
		imagefill($image, 0, 0, $transparent);

		// turning off alpha blending (to ensure alpha channel information
		// is preserved, rather than removed (blending with the rest of the
		// image in the form of black))
		imagealphablending($image, false);

		// turning on alpha channel information saving (to ensure the full range
		// of transparency is preserved)
		imagesavealpha($image, true);

		// zentriert sprite -> tile
		$sprite = imagecreatefrompng($file);
		imagealphablending($sprite, false); // setting alpha blending on
		imagesavealpha($sprite, true); // save alphablending setting (important)

		$sprite_width = imagesx($sprite);
		$sprite_height = imagesy($sprite);

		$center_y = round(($tile - $sprite_height) / 2);
		$center_x = round(($tile - $sprite_width) / 2);
		imagecopyresampled($image, $sprite, $center_x, $center_y, 0, 0, $sprite_width, $sprite_height, $sprite_width, $sprite_height);

		// copy direction tileset to full tileset
		imagecopyresampled($full_tileset, $image, $x * $tile, $y * $tile, 0, 0, $tileset_width, $tileset_height, $tileset_width, $tileset_height);

		imagedestroy($image);
		imagedestroy($sprite);
	}

	imagepng($full_tileset, $base_path . 'tileset_'.$effectname.'_240x240.png' );
	imagedestroy($full_tileset);

	$css_prefix = '.effect[data-name="[EFFECT]"] ';
	$css_template = '
[PREFIX]sprite.run
{
	animation-name: effect_[EFFECT];
	visibility: visible;
	
	animation-duration: [DURATION]ms;
	animation-iteration-count: 1;
	animation-timing-function: step-start;
	animation-fill-mode: forwards;
}

@keyframes effect_[EFFECT]
{
[KEYFRAMES]
}
';

	$animation_template = "\t" . '[PERCENT]% ' . "\t" . '{ background-position: calc(((240px * (8 * 0)) + (240px * [FRAME])) * -1 )   	calc(240px * -[Y])	}';

	$css = '
.effect[data-name="[EFFECT]"] sprite
{
	background-image: url(\'/effects/tileset_'.$effectname.'_240x240.png\');
	visibility: hidden;
}'."\n\n";

	foreach( $filled as $y => $list )
	{
		$line = str_replace('xxx', $effectname, $css_template);

		$frames = 0;
		foreach( $list as $x => $size )
		{
			if( $size > $min_size )
				$frames++;
		}

		$framelist = [];
		for ( $frame = 0; $frame < $frames; $frame++ )
		{
			$percent = round(( 100 / $frames ) * $frame, 2);

			$animation_line = str_replace('[PERCENT]', $percent, $animation_template);
			$animation_line = str_replace('[FRAME]', $frame, $animation_line);
			$animation_line = str_replace('[Y]', $y, $animation_line);

			$framelist[] = $animation_line;
		}

		// add 100%
		$animation_line = str_replace('[PERCENT]', 100, $animation_template);
		$animation_line = str_replace('[FRAME]', $frame-1, $animation_line);
		$animation_line = str_replace('[Y]', $y, $animation_line);
		$framelist[] = $animation_line;

		$line = str_replace('[KEYFRAMES]', implode("\n", $framelist), $line);
		$css .= $line;
	}

	$css = str_replace('[PREFIX]', $css_prefix, $css);
	$css = str_replace('[EFFECT]', $effectname, $css);
	$css = str_replace('[DURATION]', ( count($files) * 150 ), $css);

	file_put_contents($base_path . $effectname . '.css', $css);

	$css_mob_base .= '@import url("/effects/'.$effectname.'.css");'."\n";
}

file_put_contents($base_path . 'effects.css', $css_mob_base);