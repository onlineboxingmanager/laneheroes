"use strict";

let Entity = require('./entity');
let Utils = require('./utils');
let PF = require('pathfinding');
let path = require('path');

class Monster extends Entity
{

	/**
	 * @param game
	 * @param id
	 * @param position
	 * @param life
	 * @param speed
	 * @param is_flying
	 */
	constructor(game, player, position)
	{
		super();

		this.name = 'spider';
		this.type = 'monster';
		this.game = game;
		this.id = 1;
		this.position = position;
		this.life = 100;
		this.speed = 1000;
		this.is_flying = false;

		this.damage = [20, 25];
		this.life = 100;
		this.maxlife = 100;
		this.attack_speed = 2500;

		this.target = null;

		this.player = player;
		this.emperium = this.player.emperium;

		this.money = 5;
	};

	targetNPC()
	{
		let self = this;

		// target npc if near on next path
		console.log("monster from " + this.position + " scan for targets");
		let targets = [];

		let distance = 0;
		let chars;
		for( let i in this.player.chars )
		{
			let char = this.player.chars[i];
			if( char.life <= 0 )
				continue;

			targets.push( char );

			console.log(char.position);
			console.log(char.player.playerid );
		}

		// attack first monster
		if( targets.length > 0 )
			this.walkTo(targets[0], function()
			{
				self.attack(targets[0], function()
				{
					self.targetNPC();
				});
			});

		// scan for others
		else
			this.targetEmperium();

	};

	targetEmperium()
	{
		let self = this;

		console.log("monster from " + this.position + " target emp on " + this.player.emperium.position);
		this.walkTo(this.player.emperium, function()
		{
			self.attack(self.emperium);
		});
	};

};

module.exports = Monster;