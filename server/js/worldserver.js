let BaseClass = require('./lib/class.js');
let Utils = require('./utils');
let path = require('path');
let Player = require('./player.js');
let Bot = require('./bot.js');
let Game = require('./game.js');

class WorldServer extends BaseClass
{
	constructor(io)
	{
		super();

		let self = this;

		this.total_players = 0;
		this.clients = {};
		this.games = {};
		this.io = io;

		this.lobbies = {};

		console.log("create worldserver");

		this.io.on('connection', (socket) =>
		{
			self.onConnect(socket);

			socket.on('disconnect', (reason) =>
			{
				self.onDisconnect(socket, reason);
			});

			socket.on('check lobby', (lobbyid) =>
			{
				self.checkLobby(socket, lobbyid);
			});

			socket.on('create lobby', (player) =>
			{
				if( player.playername.length === 0 )
					return false;

				self.createLobby(socket, player.playername);
			});

			socket.on('start game', (modus) =>
			{
				self.createGame(socket, modus);
			});

			/*debug*/
			socket.conn.on('packet', function (packet) {
				//console.log("server request by client");
				//console.log(packet);
				if (packet.type === 'ping') console.log('received ping');
			});

			/*debug*/
			socket.conn.on('packetCreate', function (packet) {
				//console.log("server response to clients");
				//console.log(packet);
				if (packet.type === 'pong') console.log('sending pong');
			});

		});
	};

	onConnect(socket)
	{
		this.total_players++;

		this.sendTotalPlayers(socket);

		// debug - direct start game
		this.createLobby(socket, "test");
		this.createGame(socket, "singleplayer")

		this.io.sockets.emit('user connected', socket.client.id);
		console.log('a user connected ' + socket.client.id );
	};

	onDisconnect(socket, reason)
	{
		this.total_players--;
		this.sendTotalPlayers(socket);

		this.io.sockets.emit('user disconnected', socket.client.id);
		console.log('user disconnect ' + socket.client.id + " / reason: " + reason);

		delete this.clients[socket.client.id];

		this.updateClients(socket);

		for( let lobbyid in this.lobbies )
			this.sendLobbyStatus(lobbyid);

		//this.games[socket.client.id].end();
		//delete this.games[socket.client.id];
		// clear all opened timeouts for this game

		this.endGames();

	};

	createLobby(socket, playername)
	{
		let myPlayer = new Player(socket);
		myPlayer.playername = playername;

		// add player to client list
		this.clients[socket.client.id] = myPlayer;

		// join existing lobby
		let lobbyid = socket.handshake.query.lobbyid;
		if( lobbyid && this.lobbies[lobbyid] )
		{
			// add client to lobby
			this.lobbies[lobbyid].push( socket.client.id );

			// update client
			this.clients[socket.client.id].lobbyid = lobbyid;

			//console.log("join lobby");
			socket.emit("lobby joined", lobbyid );
			socket.join(lobbyid);

			this.sendLobbyStatus(lobbyid);
			this.updateClients(socket);

			return;
		}

		// create new lobby id
		lobbyid = Utils.randomString(5, '0123456789abcdef');

		// clients in lobby
		this.lobbies[lobbyid] = [];

		// selbst hinzufügen
		this.lobbies[lobbyid].push( socket.client.id );

		console.log("lobby created [" + lobbyid + "]");
		socket.emit("lobby created", lobbyid );

		// update client
		this.clients[socket.client.id].lobbyid = lobbyid;

		socket.join(lobbyid);

		this.sendLobbyStatus(lobbyid);
		this.updateClients(socket);
	};

	checkLobby(socket, lobbyid)
	{
		console.log("check lobby " + lobbyid);

		if( !this.lobbies[lobbyid] )
			socket.emit("redirect to", "/");
	};

	sendLobbyStatus(lobbyid)
	{
		// output list
		let list = [];
		for( let i in this.lobbies[lobbyid] )
		{
			let socketid = this.lobbies[lobbyid][i];

			if( !this.clients[socketid] )
			{
				delete this.lobbies[lobbyid][i];
				continue;
			}

			list.push( { playername: this.clients[socketid].playername } );
		}

		for( let i in this.lobbies[lobbyid] )
		{
			let socketid = this.lobbies[lobbyid][i];
			this.io.sockets.to(lobbyid).emit('client list', list);
		}
	};

	updateClients(socket)
	{
		this.sendTotalLobbies(socket);
		this.sendTotalPlayers(socket);
	};

	createGame(socket, modus)
	{
		// end other games
		//this.endGames();

		let lobbyid = this.clients[socket.client.id].lobbyid;

		// re-join opened game
		if( this.games[lobbyid] )
		{
			//this.game[lobbyid].sendMap(socket);
			delete this.games[lobbyid];
			return false;
		}

		this.io.to(lobbyid).emit("load game");

		let players = [];

		for( let i in this.lobbies[lobbyid] )
		{
			let socketid = this.lobbies[lobbyid][i];

			console.log("socket id: " + socketid);

			if( this.clients[socketid] )
				players.push( this.clients[socketid] );
		}

		// add bot
		if( modus === 'singleplayer' && players.length === 1 )
		{
			let myBot = new Bot(null);
			players.push( myBot );
		}

		console.log( "start game for lobby [" + lobbyid + "] (modus " + modus + ") for total players: " + players.length );

		// start game
		this.games[lobbyid] = new Game(this, players, lobbyid);
	};

	sendTotalPlayers(socket)
	{
		this.io.sockets.emit('total players', this.total_players);
	};

	sendTotalLobbies(socket)
	{
		this.io.sockets.emit('total lobbies', Object.keys(this.lobbies).length );
	};

	endGames()
	{
		for ( let lobbyid in this.games )
		{
			if( this.games[lobbyid] )
			{
				this.games[lobbyid].end();
				delete this.games[lobbyid];

				delete this.lobbies[lobbyid];
			}

		}

		//delete this.lobbies;
	};

};

module.exports = WorldServer;