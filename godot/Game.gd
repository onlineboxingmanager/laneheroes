class_name ExampleClient extends Node

var client = SocketIOClient
var backendURL: String

var items = {}

var camera_position = 0
var camera_position_origin = 0

func _ready():
				
	# prepare URL
	backendURL = "ws://localhost:3000/socket.io"

	# initialize client
	client = SocketIOClient.new(backendURL)

	# this signal is emitted when the socket is ready to connect
	client.on_engine_connected.connect(on_socket_ready)

	# this signal is emitted when socketio server is connected
	client.on_connect.connect(on_socket_connect)

	# this signal is emitted when socketio server sends a message
	client.on_event.connect(on_socket_event)

	# add client to tree to start websocket
	add_child(client)

func _exit_tree():
	# optional: disconnect from socketio server
	client.socketio_disconnect()

func on_socket_ready(_sid: String):
	# connect to socketio server when engine.io connection is ready
	client.socketio_connect()

func on_socket_connect(_payload: Variant, _name_space, error: bool):
	if error:
		push_error("Failed to connect to backend!")
	else:
		print("Socket connected")

func on_socket_event(event_name: String, payload: Variant, _name_space):
	print("Received ", event_name, " ", payload)
	
	# Check if the event is "draw item"
	if event_name == "spawn entity":
		draw_item(payload);
		
	elif event_name == "move entity":
		move_item(payload);
		
	elif event_name == "attack entity":
		attack(payload);
		
	elif event_name == "hit entity":
		hit(payload);
		
	elif event_name == "dead entity":
		dead(payload);
		
	elif event_name == "player status":
		player_status(payload);

func player_status(data):
	var playerid: int = int(data.playerid) + 1
	var money: int = data.money
	var name: String = data.playername
	
	var name_label: Label = get_tree().get_current_scene().get_node("Camera2D/UI Player " + str(playerid) + "/Name PL" + str(playerid));
	if( name_label):
		name_label.text = name
	
	var money_label: Label = get_tree().get_current_scene().get_node("Camera2D/UI Player " + str(playerid) + "/Money PL" + str(playerid));
	if( money_label):
		money_label.text = str(money) + ' G'
	

# Funktion zum Erstellen eines neuen 2D-Objekts auf der Karte
func draw_item(data):
	# Hier kannst du die Logik implementieren, um ein neues 2D-Objekt auf der Karte zu erstellen
	# Verwende die UID, um das Objekt eindeutig zu identifizieren
	print("draw 2D object: ", data.uid)
	
	var life: int = data.l
	var mana: int = data.m
	var speed: int = data.s
	var name: String = data.n
	var type: String = data.t

	var map = get_tree().get_current_scene()

	var scene = load("res://player.tscn")
	var char: Node = scene.instantiate()

	char.name = "item_" + String(data.uid)
	items[data.uid] = char # assign to global list
		
	char.position = Vector2(data.p[1] * 32, data.p[0] * 32)
	char.visible = true
	
	# add char to scene
	map.add_child(char)
	
	# play idle animation
	var animation: AnimatedSprite2D = char.get_child(0)
	animation.play("IDLE_DOWN")
	
	# set healthbar
	var healthbar = char.find_child("Healthbar")
	char.set_health(life)

func move_item(data):

	var char: Area2D = items[data.uid]
	var new_pos = Vector2(data.p[1] * 32, data.p[0] * 32)
	var speed: int = data.s
		
	var tween = create_tween() # Creates a new tween
	tween.tween_property(char, "position", new_pos, speed / 1000)
	
	var animation: AnimatedSprite2D = char.get_child(0)
	
	if( data.d == "n" || data.d == "nw" || data.d == "no" ):
		animation.play("WALK_UP")
	elif( data.d == "o" || data.d == "so" ):
		animation.play("WALK_RIGHT")
	elif( data.d == "w" || data.d == "sw"  ):
		animation.play("WALK_LEFT")
	elif( data.d == "s" ):
		animation.play("WALK_DOWN")
		
func attack(data):
	var char: Area2D = items[data.uid]
	var animation: AnimatedSprite2D = char.get_child(0)
	var speed: int = data.s
	var life: int = data.l
	var mana: int = data.m
	var type: String = data.t
	var name: String = data.n
	
	if( data.d == "o" || data.d == "so" || data.d == "no" ):
		animation.play("THRUST_RIGHT")
	elif( data.d == "w" || data.d == "sw" || data.d == "nw" ):
		animation.play("THRUST_LEFT")
	elif( data.d == "s" ):
		animation.play("THRUST_DOWN")
	elif( data.d == "n" ):
		animation.play("THRUST_UP")

func hit(data):
	var char: Area2D = items[data.uid]
	var animation: AnimatedSprite2D = char.get_child(0)
	var damage_number: Node = char.get_node("DamageNumber")
	var hit_flash: AnimationPlayer = char.get_node("HitFlashAnimationPlayer")
	hit_flash.play("hit_flash")
	var life: int = data.l
	
	var position = char.position
	position.y -= 5
	
	var damage = data.dmg
	DamageNumbers.display_number(damage, position, false)
	
	# set healthbar
	var healthbar = char.find_child("Healthbar")
	char.set_health(life)
		
	char.on_hit()

func dead(data):
	var char: Area2D = items[data.uid]
	if data.t == "monster":
		char.queue_free()
		items[data.uid] = null

func _on_button_camera_pressed():
	var camera: Camera2D = get_tree().get_current_scene().get_node("Camera2D")
		
	camera_position += 1
	if camera_position >= 2:
		camera_position = 0
		
	if camera_position == 0:
		camera.global_position.x -= get_viewport().size.x
	if camera_position == 1:
		camera.global_position.x += get_viewport().size.x
			
	print("pressed", camera)

