"use strict";

let BaseClass = require('./lib/class.js');
let Utils = require('./utils');

class Player extends BaseClass
{
	constructor(socket)
	{
		super();

		if( !socket )
			socket = { on:function(){}, emit: function(){}, client: { id: null }};

		this.socket = socket;

		this.money = 110;

		this.playername;
		this.emperium;

		this.zones = {};
		this.zones.mobs = {};
		this.zones.defender = {};
		this.zones.versus = {};

		this.chars = [];
		this.monsters = [];
	}

	doBot()
	{
		return false;

		const pos = [Utils.randomInt(1,31), Utils.randomInt(1,16)];
		this.game.buy(this, pos, 'novice');
	};

	getState()
	{
		return {
			playerid: this.playerid,
			money: this.money,
			playername: this.playername
		}
	}

}

module.exports = Player;