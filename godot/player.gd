extends Area2D

@onready var healthbar = $Healthbar

# Called when the node enters the scene tree for the first time.
func _ready():
	healthbar.init_health(100)

func set_health(value):
	healthbar._set_health(value)
	
func on_hit():
	if( randi_range(1, 2) == 1 ):
		$SFX_HIT.play()
	else:
		$SFX_HIT2.play()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
