"use strict";

let Player = require('./player.js');
let Utils = require('./utils');

class Bot extends Player
{
	constructor(socket, game)
	{
		super();

		if( !socket )
			socket = { on:function(){}, emit: function(){}, client: { id: null }};

		this.socket = socket;

		this.money = 110;

		this.game = game;

		this.playername = "Bot";
		this.emperium;

		this.zones = {};
		this.zones.mobs = [];
		this.zones.defender = {};
		this.zones.versus = {};

		this.chars = [];
		this.monsters = [];
	}

	doBot()
	{
		console.log("do bot things at round " + this.game.round);

		if( this.game.round == 1 )
		{
			let pos = this.zones.defender[10];
			this.game.buy(this, pos, 'novice');
		}

		if( this.game.round == 2 )
		{
			let pos = this.zones.defender[10];
			this.game.upgrade(this, pos, 'sword');
		}

		if( this.game.round == 3 )
		{
			let pos = this.zones.defender[15];
			this.game.buy(this, pos, 'novice');
		}

		if( this.game.round == 5 )
		{
			let pos = this.zones.defender[15];
			this.game.upgrade(this, pos, 'archer');
		}

		if( this.game.round == 6 )
		{
			let pos = this.zones.defender[12];
			this.game.buy(this, pos, 'novice');
		}

		if( this.game.round == 8 )
		{
			let pos = this.zones.defender[12];
			this.game.upgrade(this, pos, 'mage');
		}

		if( this.game.round == 9 )
		{
			let pos = this.zones.defender[13];
			this.game.buy(this, pos, 'novice');
		}

		if( this.game.round == 10 )
		{
			let pos = this.zones.defender[13];
			this.game.upgrade(this, pos, 'thief');
		}

	};

}

module.exports = Bot;