<?php
#�

$base_path = __DIR__ . '/../sprites/character';

$config = array();
$config['gender'] = array(0, 1);
$config['direction'] = array(0, 1, 2, 3, 4, 5, 6, 7);
$config['action'] = array
(
	0 => array( 0 ), // stehen
	1 => array( 0, 1, 2, 3, 4, 5, 6, 7 ), // laufen
	2 => array( 0 ), // sitzen
	3 => array( 0, 1, 2 ), // aufheben
	4 => array( 0, 1, 2, 3, 4, 5 ), // kampfhaltung 1
	5 => array( 0, 1, 2, 3, 4 ), // angriff
	6 => array( 0, 1, 2 ), // selbst getroffen
	7 => array( 0 ), // sterben
	8 => array( 0 ), // tot
	9 => array( 0 ), // alt attack 1
	10 => array( 0, 1, 2, 3, 4, 5, 6, 7, 8 ), // alt attack 2
	11 => array( 0, 1, 2, 3, 4, 5, 6, 7 ), // alt attack 3
	12 => array( 0, 1, 2, 3, 4, 5 ), // zaubern
);


$tile = 240; // feste gr��e der Sprites (240x240)

$tileset_width = $tile * 9 * 8; // Tile x Richtungen x Frames max.
$tileset_height = $tile * count($config['action']);

$characters = glob($base_path . '/*', GLOB_ONLYDIR);

foreach( $characters as $folder )
{
	$tileset_file = $base_path . '/' . basename($folder) . '.png';
	if( file_exists($tileset_file) )
		continue;

	$full_tileset = imagecreatetruecolor( $tileset_width, $tileset_height );
	imagesavealpha($full_tileset, true);
	
	$full_tileset_color = imagecolorallocatealpha($full_tileset, 0, 0, 0, 127);
	imagefill($full_tileset, 0, 0, $full_tileset_color);

	foreach( $config['action'] as $action_key => $framelist )
	{
		$pos_y = $action_key * $tile; // h�he
		$pos_x = 0; // breite
		
		foreach( $config['direction'] as $direction )
		{
			
			foreach( $framelist as $frame )
			{
				
				$file = $folder . '/direction_'.$direction.'__action_'.$action_key.'__frame_'.$frame.'.png';
				$sprite = imagecreatefrompng($file);
				
				imagecopy($full_tileset, $sprite, $pos_x, $pos_y, 0, 0, $tile, $tile);
				
				$pos_x += $tile;
			}
			
		}	
		
		#if( $action_key > 1 )
		#break; // debug
	}
	
	imagepng($full_tileset, $tileset_file );
	imagedestroy($full_tileset);

	/*
	foreach( $config['direction'] as $direction )
	{
		
		$image = imagecreatetruecolor( $tileset_width, $tileset_height );
		imagesavealpha($image, true);
		imagealphablending($image, false);
		
		$color = imagecolorallocatealpha($image, 0, 0, 0, 127);
		imagefill($image, 0, 0, $color);
	
		foreach( $config['action'] as $action_key => $framelist )
		{
			foreach( $framelist as $frame )
			{
				
				$file = $folder . '/direction_'.$direction.'__action_'.$action_key.'__frame_'.$frame.'.png';
				
				$sprite = imagecreatefrompng($file);

				// save transparency
				#imagealphablending($sprite, false);
				#imagesavealpha($sprite, false);

				#imagecopyresampled($image, $sprite, $tile * $frame, $tile * $action_key, 0, 0, $tile, $tile, $tile, $tile); // nicht gut
				imagecopy($image, $sprite, $tile * $frame, $tile * $action_key, 0, 0, $tile, $tile);
				
			}
		}
		
		$tileset = $folder . '/tileset_' . $direction . '.png';
		imagepng($image, $tileset);
		
		// copy direction tileset to full tileset
		imagecopy($full_tileset, $image, 0, $i * $tileset_height, 0, 0, $tileset_width, $tileset_height);
		
		imagedestroy($image);

		
		$i++;
	}
	
		imagepng($full_tileset, $folder . '/tileset.png' );
	imagedestroy($full_tileset);
*/

}