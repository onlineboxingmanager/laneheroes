let Entity = require('./entity');
let Messages = require('./message');
let Utils = require('./utils');
let PF = require('pathfinding');
let path = require('path');

class Emperium extends Entity
{

	constructor(game, player, position)
	{
		super();

		this.type = 'emperium';
		this.game = game;
		this.player = player;
		this.team = player.team;
		this.position = position;
		this.life = 10000;
		this.maxlife = this.life;
		this.armor = 0;
		this.evasion = 0;
		this.speed = 0;
		this.attack_speed = 2000;
		this.damage = [100,200];
		this.range = 1;
	};

	attackNearby()
	{
		let self = this;

		//console.log("emp scan for targets");

		clearTimeout(this.timer);
		this.timer = setTimeout(function()
		{
			let targets = [];

			let distance = 0;
			for( let i in self.player.monsters)
			{
				let monster = self.player.monsters[i];
				if( monster.life === 0 )
					continue;

				distance = Utils.distanceTo( monster.position[0], monster.position[1], self.position[0], self.position[1] );
				if( distance <= self.range )
					targets.push( monster );
			}

			// attack first monster
			if( targets.length > 0 )
				self.attack(targets[0], function()
				{
					self.attackNearby();
				});

			// scan for others
			else
				self.attackNearby();

		}, this.attack_speed);
	}

};

module.exports = Emperium;