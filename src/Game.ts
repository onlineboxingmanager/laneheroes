import {Socket} from "socket.io-client";
import {getRandomInt} from "./Client";
import Player from "./Player";
import Types from "./GameTypes";
import * as THREE from 'three';
import {WebGLRenderer} from "three/src/renderers/WebGLRenderer";
import {Camera, CameraHelper, Clock, Mesh, Scene, Sprite, sRGBEncoding} from "three";
import SpriteTileset from "./SpriteTileset";
import Stats from 'three/examples/jsm/libs/stats.module'
import {OrbitControls} from "three/examples/jsm/controls/OrbitControls";
import { CSS2DRenderer, CSS2DObject } from 'three/addons/renderers/CSS2DRenderer.js';
import { GLTFLoader } from 'three/addons/loaders/GLTFLoader.js';

import { GUI } from 'dat.gui'
import {Spore} from "./lib/mobs/Spore";
import {TrackballControls} from "three/examples/jsm/controls/TrackballControls";
import {MapControls} from "three/examples/jsm/controls/MapControls";
import { gsap } from "gsap";

export default class Game
{

    private tilesize: number = 32
    private grid: number = 32
    private map_size = [64, 64]

    private myPlayer: Player

    private socket: Socket

    private lobby: HTMLElement

    private map: HTMLElement
    private camera_element: HTMLElement
    private game_start: HTMLElement
    private game_over: HTMLElement

    private engine: any
    private fps: number = 30
    private scene: Scene
    private renderer: WebGLRenderer
    private labelRenderer: CSS2DRenderer
    public camera: Camera
    public mixer

    private stats: any
    private clock: Clock
    private delta: number = 0
    private width: number = 0
    private height: number = 0

    private entities: Map<number, SpriteTileset> = new Map();
    private controls: OrbitControls;
    private gui: GUI;
    private cameraHelper: CameraHelper;

    public models = {
        'knight': null
    }

    constructor(socket: Socket)
    {
        // TODO:
        // https://pixijs.com/
        // https://www.reddit.com/r/threejs/comments/10efgd6/is_threejs_just_as_good_for_2d_gamesgraphics_or/


        this.engine = THREE;
        this.scene = new THREE.Scene();
        this.scene.add(new THREE.AxesHelper(5))
        this.clock = new THREE.Clock();

        this.setRenderer()
        //this.setCamera( new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 1, 1000 ) );
        this.setCamera();

        this.gui = new GUI()

        this.controls = new OrbitControls( this.camera, this.renderer.domElement );
        this.controls.listenToKeyEvents( window ); // optional

        //controls.addEventListener( 'change', render ); // call this only in static scenes (i.e., if there is no animation loop)

        this.controls.enableDamping = true; // an animation loop is required when either damping or auto-rotation are enabled
        this.controls.dampingFactor = 0.05;

        this.controls.screenSpacePanning = false;

        this.controls.minDistance = 1;
        this.controls.maxDistance = 100;

        //this.controls.maxPolarAngle = Math.PI / 2;

        // https://threejs.org/examples/?q=controls#misc_controls_map
        /*
        this.controls = new MapControls( this.camera, this.renderer.domElement );

        this.controls.enableDamping = true; // an animation loop is required when either damping or auto-rotation are enabled
        this.controls.dampingFactor = 0.05;
        this.controls.screenSpacePanning = false;

        this.controls.minPolarAngle = 0.5;
        this.controls.maxPolarAngle = 2.5;
        this.controls.minAzimuthAngle = 0
        this.controls.maxAzimuthAngle = 0
        this.controls.maxZoom = 20
        this.controls.minZoom = 0
        this.controls.minDistance = 0
        this.controls.maxDistance = 20
        this.controls.zoomSpeed = 1.0
        this.controls.rotateSpeed = 2.0
        this.controls.panSpeed = 0.8
        this.controls.enablePan = false
        this.controls.addEventListener('change', () => this.gui.updateDisplay())
        this.gui.add(this.controls, 'maxPolarAngle')
        this.gui.add(this.controls, 'maxZoom')
        this.gui.add(this.controls, 'maxDistance')
        this.gui.add(this.controls, 'maxPolarAngle')
        this.gui.add(this.controls, 'zoomSpeed')
        this.gui.add(this.controls, 'rotateSpeed')
        this.gui.add(this.controls, 'enablePan')
        this.gui.add(this.controls, 'enableDamping')
        this.gui.add(this.controls, 'screenSpacePanning')
        */

        this.camera.position.set( 0, 0, this.zoom );
        this.camera.addEventListener('change', () => this.gui.updateDisplay())

        this.stats = new Stats()
        document.body.appendChild(this.stats.dom)

        console.log('engine loaded', this.engine)

        this.socket = socket

        this.map = <HTMLElement> document.getElementById('map');
        this.camera_element = <HTMLElement> document.getElementById('camera');
        this.lobby = document.getElementById('lobby');
        this.game_start = document.getElementById('game_start');
        this.game_over = document.getElementById('game_over');

        this.binds();

        let audiotimer = {};

        this.loadModels()

        /*
        for( let skillid in Types.SKILLS )
        {
            let skill = Types.SKILLS[skillid];

            let duration = skill.tileset ? skill.tileset[2] : 3000;

            let template = '<div id="" class="effect" data-name="' + skillid + '" data-duration="' + duration + '">' +
                '<sprite class=""></sprite>' +
                '<audio controls preload="auto" data-duration="' + skill.audio[2] + '" data-start="' + skill.audio[1] + '"><source src="/audio/' + skill.audio[0] + '" /></audio>' +
                '</div>';

            document.querySelector(".skills").innerHTML += template;
        }
        */
    }

    async loadModels()
    {
        THREE.Cache.enabled = true;

        // model
        const loader = new GLTFLoader();
        loader.load( '/src/lib/Knight.glb',  ( gltf ) =>
        {
            this.models.knight = gltf
            console.log(gltf)
        } );
        loader.load( '/src/lib/Mage.glb',  ( gltf ) =>
        {
            this.models.mage = gltf
            console.log(gltf)
        } );
    }

    setCamera()
    {
        const width = window.innerWidth;
        const height = window.innerHeight;

        const camera = new THREE.PerspectiveCamera( 45, window.innerWidth / window.innerHeight, 1, 1000 );

        this.camera = camera
        this.scene.add(camera);
        console.log("add camera", camera)

        this.cameraHelper = new THREE.CameraHelper(camera);
        this.scene.add(this.cameraHelper)

        let ambienteLight = new THREE.AmbientLight(0x555555);
        this.scene.add(ambienteLight);

        let light = new THREE.DirectionalLight( 0xffffff, 0.75 );
        light.position.set(0, 0, 1);
        this.scene.add( light );
    }

    setRenderer()
    {
        this.scene.fog = new this.engine.FogExp2(0x11111f, 0.002);

        this.renderer = new THREE.WebGLRenderer({ antialias: true });
        this.renderer.setPixelRatio( window.devicePixelRatio ); // TODO: Use player.setPixelRatio()
        this.renderer.setSize( window.innerWidth, window.innerHeight );
        this.renderer.setClearColor(this.scene.fog.color);
        this.renderer.outputColorSpace = THREE.SRGBColorSpace; // optional with post-processing

        //this.renderer.outputEncoding = this.engine.sRGBEncoding;

        //this.renderer.xr.enabled = false;
        //this.renderer.shadowMap.enabled = true;
        //this.renderer.shadowMap.type = 1;
        //this.renderer.toneMapping = 0;
        //this.renderer.toneMappingExposure = 1;
        //this.renderer.physicallyCorrectLights = false;

        let game_container = document.getElementById('game')
        this.renderer.domElement.setAttribute('id', 'map_canvas')
        game_container.prepend( this.renderer.domElement );


        this.labelRenderer = new CSS2DRenderer();
        this.labelRenderer.setSize( window.innerWidth, window.innerHeight );
        this.labelRenderer.domElement.style.position = 'absolute';
        this.labelRenderer.domElement.style.top = '0px';
        this.labelRenderer.domElement.style.pointerEvents = 'none';
        this.labelRenderer.domElement.setAttribute('id', 'label_canvas')
        game_container.appendChild( this.labelRenderer.domElement );

        //const controls = new OrbitControls( this.camera, this.labelRenderer.domElement );
        //controls.minDistance = 5;
        //controls.maxDistance = 100;
    }

    render()
    {
        this.renderer.render( this.scene, this.camera );
        this.labelRenderer.render( this.scene, this.camera );
    }

    update(deltaTime?: number)
    {
        // Update controls
        this.controls?.update();

        // jedes einzelne element update funktion ausführen
        this.entities.forEach(e => e.update(deltaTime));

        this.updateCamera();
        this.cameraHelper.update();
        this.stats?.update();
    }

    updateCamera()
    {
        // @ts-ignore
        this.camera?.updateProjectionMatrix();
    }

    play()
    {
        this.animate();
    }

    animate(delta?: number)
    {
        this.delta += this.clock.getDelta();

        let interval = (1 / this.fps);
        requestAnimationFrame( () => this.animate(this.delta) );

        if ( this.delta > interval )
        {
            // The draw or time dependent code are here
            this.update(this.delta);
            this.render();

            this.delta = this.delta % interval;
        }
    }

    setSize(width, height)
    {
        this.width = width;
        this.height = height;

        console.log("set size")

        if ( this.camera )
        {
            this.camera.aspect = this.width / this.height;
            this.camera.updateMatrix();
        }

        this.renderer.setSize( width, height );
    }

    resize()
    {
        this.setSize( window.innerWidth, window.innerHeight );
    }

    randomIntFromInterval(min, max) { // min and max included
        return Math.floor(Math.random() * (max - min + 1) + min)
    }

    private binds()
    {
        window.addEventListener( 'resize', () =>
        {
            this.resize();
        });

        document.getElementById("map_canvas").addEventListener("mousemove", (event) =>
        {
            let x = Math.floor(event.offsetX / this.tilesize);
            let y = Math.floor(event.offsetY / this.tilesize);

            this.highlightArea([y, x]);
        });

        this.socket.on("map show", (json) =>
        {
            this.drawMap(json);

            this.camera_element.style.display = "block";
            this.lobby.style.display = "none";
            this.game_start.style.display = "none";
        });

        this.socket.on('game start', (player) =>
        {
            this.lobby.style.display = "none";
            this.game_start.style.display = "none";

            document.querySelectorAll('.monster').forEach(e => e.remove());
            document.querySelectorAll('.emperium').forEach(e => e.remove());
            document.querySelectorAll('.char').forEach(e => e.remove());
            document.querySelectorAll('.area_defender').forEach(e => e.remove());

            document.getElementById("player_" + player.playerid ).querySelector('.playername').innerText = player.playername;
            document.getElementById("player_" + player.playerid ).querySelector('.playerid').innerText = player.playerid;

            this.myPlayer = player;

            this.camera_element.className = "player-" + player.playerid;
        });

        this.socket.on('game over', (player) =>
        {
            this.lobby.style.display = "none";
            this.camera_element.style.display = "none";
            this.game_start.style.display = "none";
            this.game_over.style.display = "block";

            document.querySelectorAll('.monster').forEach(e => e.remove());
            document.querySelectorAll('.emperium').forEach(e => e.remove());
            document.querySelectorAll('.char').forEach(e => e.remove());
            document.querySelectorAll('.area_defender').forEach(e => e.remove());
        });

        this.socket.on('round start', (msg) =>
        {
            document.querySelectorAll('.phase').forEach( e => e["innerText"] = 'Vorbereitung');
            document.querySelectorAll('.round').forEach( e => e["innerText"] = msg);
            document.getElementById('game').className = 'preperation';

            document.querySelectorAll('.show-upgrades').forEach(e => e.classList.remove("show-upgrades")); // close popups
        });

        this.socket.on('wave start', (msg) =>
        {
            document.querySelectorAll('.phase').forEach( e => e["innerText"] = 'WAVE');
            document.querySelectorAll('.time').forEach( e => e["innerText"] = '');

            document.getElementById('game').className = 'wave';

            document.querySelectorAll('.show-upgrades').forEach(e => e.classList.remove("show-upgrades")); // close popups
        });

        this.socket.on('start versus', (msg) =>
        {
            document.querySelectorAll('.phase').forEach( e => e["innerText"] = 'VERSUS');
            document.getElementById('game').className = 'versus';
        });

        this.socket.on('player status', (player) =>
        {
            let id = parseInt(player.playerid) + 1;
            let item = document.getElementById('player_' + id);

            item.querySelector('.money')["innerText"] = parseInt(player.money);
            item.querySelector('.playername')["innerText"] = player.playername;
        });

        this.socket.on('set timer', (msg) =>
        {
            let duration = parseInt(msg);

            let interval = setInterval(() =>
            {
                duration -= 1000;
                document.querySelectorAll('.time').forEach( e => e["innerText"] = (duration / 1000) + 's' );

                if( duration <= 0 )
                {
                    clearInterval(interval);
                    return false;
                }

            }, 1000 );

        });

        this.socket.on('spawn entity', (msg) =>
        {
            this.drawItem(msg, 'wait', 'spawn');
        });

        this.socket.on('despawn entity', (msg) =>
        {
            const entity = this.entities[msg.uid];
            this.scene.remove(entity)
        });

        this.socket.on('stop entity', (msg) => {
            setTimeout(() =>
            {
                this.drawItem(msg, 'wait');
            }, 250); // must be at speed time
        });

        this.socket.on('attack entity', (msg)  =>
        {
            setTimeout(() =>
            {
                // reset mode to wait after one attack
                setTimeout(() =>
                {
                    this.drawItem(msg, 'wait', null);
                }, 950);

                this.drawItem(msg, 'attack', null);
            }, 0); // must be at speed time
        });

        this.socket.on('cast', (msg) =>
        {
            this.drawItem(msg, 'cast', null);
        });

        this.socket.on('hit entity', (msg) =>
        {
            this.showDamage(msg);
        });

        this.socket.on('heal entity', (msg) =>
        {
            this.showDamage(msg);
        });

        this.socket.on('miss entity',  (msg) =>
        {
            msg.dmg = 'miss';
            this.showDamage(msg);
        });

        this.socket.on('skill use', (msg) =>
        {
            this.runEffect(msg[0], msg[1]);

            if( msg[2] )
                this.drawItem(msg[2], 'use');
        });

        this.socket.on('dead entity', (msg) =>
        {
            this.showDead(msg)
        });

        this.socket.on('move entity', (msg) =>
        {
            this.drawItem(msg, 'walk');
        });
    }

    public showDead(msg)
    {
        let myEntity = this.entities.get(msg.uid);
        myEntity.model.visible = false;
    }

    public showHeal(msg)
    {
        return

        let element = document.getElementById(msg.uid);
        let dmgbox = <HTMLElement> element.querySelector('.dmg');
        //dmgbox.style.left = parseInt(msg.p[1] * tilesize) + "px";
        //dmgbox.style.top = parseInt(msg.p[0] * tilesize) + "px";
        dmgbox.style.marginLeft += (getRandomInt(0, 3) * -1) + 'px';
        dmgbox.style.marginTop += (getRandomInt(0, 3) * -1) + 'px';
        dmgbox.innerText = msg.dmg;
        dmgbox.className = "dmg show heal";

        setTimeout(() => {
            dmgbox.className = "dmg";
        }, 1000);
    };

    public showDamage(msg)
    {
       let myEntity = this.entities.get(msg.uid) ?? null;
       gsap.to(myEntity.model?.material?.color, { duration: 0.2, r: 1, g: 1, b: 1, yoyo: true, repeat: 1 });

       myEntity.lifebar.element.innerText = msg.l;
    };

    public drawItem(msg, mode: string, type?: any)
    {

        if ( !msg.uid )
            return false;

        console.log("draw item [" + msg.uid + "] in mode ["+ mode +"]:", msg);

        const tilesize = this.tilesize;
        let direction = msg.d;
        let position: Array<number> = msg.p;
        let myEntity: SpriteTileset = this.entities.get(msg.uid) ?? null
        let target = this.entities.get(msg.ta);

        let tilesetCols = 8;
        let tilesetRows = 8;

        let playerid = parseInt(msg.pl) + 1;

        let entity = Types.CHARS.king3;
        if (playerid == 2) {
            entity = Types.CHARS.king2;
        }

        if (msg.t == 'char')
        {
            entity = Types.CHARS[msg.n];
            tilesetCols = 48;
            tilesetRows = 13;
        }

        if (msg.t == 'monster')
        {
            tilesetCols = 8;
            tilesetRows = 8;
            entity = Types.MOBS[msg.n];
        }

        // object not exists in scene
        if ( !myEntity )
        {

            myEntity = new SpriteTileset(entity, tilesetRows, tilesetCols, this.tilesize, this)
            myEntity.model.scale.set(1, 1, 1)
            myEntity.model.uuid = msg.uid;

            if( msg.pl == 1 )
                myEntity.model.scale.x = -1;

            myEntity.model.layers.enableAll();
            this.scene.add( myEntity.model );

            // LIFEBAR
            const lifebar_div = document.createElement( 'div' );
            lifebar_div.className = 'lifebar';
            lifebar_div.style.backgroundColor = 'transparent';

            const lifebar = new CSS2DObject( lifebar_div );
            lifebar.position.set( -0.5, 0, 1 );
            lifebar.center.set( 0, 1 );
            myEntity.model.add( lifebar );
            lifebar.layers.set( 0 );
            myEntity.lifebar = lifebar;

            // State - debug
            const state_div = document.createElement( 'div' );
            state_div.className = 'state';
            state_div.style.backgroundColor = 'transparent';

            const state = new CSS2DObject( state_div );
            state.position.set( 0, -0.1, 1 );
            state.center.set( 0, 1 );
            myEntity.model.add( state );
            state.layers.set( 0 );

            myEntity.state = state;

            // State - debug
            const faction_div = document.createElement( 'div' );
            faction_div.className = 'faction';
            faction_div.style.backgroundColor = msg.pl == 0 ? 'blue' : 'red';
            faction_div.innerText = msg.pl == 0 ? 'blue' : 'red'

            const faction = new CSS2DObject( faction_div );
            faction.position.set( -1, -1, 1 );
            faction.center.set( 0, 1 );
            myEntity.model.add( faction );
            faction.layers.set( 0 );

            myEntity.faction = faction;


            this.entities.set(msg.uid, myEntity); // saved with uuid
            console.log("create new entity", myEntity);

            // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ //
            // audio
            // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ //
            /*
            let audio = '';
            for (let audiotype in entity.audio) {
                audio += '<audio controls preload="auto" class="' + audiotype + '" data-start="' + parseFloat(entity.audio[audiotype][1]) + '" data-duration="' + parseFloat(entity.audio[audiotype][2]) + '"><source src="/audio/' + entity.audio[audiotype][0] + '" /></audio>';
            }

            if (playerid == this.myPlayer.playerid)
                template = template.replace('[AUDIO]', audio);
            else
                template = template.replace('[AUDIO]', '');
            */

            // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ //
            // upgrades
            // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ //
            /*
            let upgrades = '';

            if (entity.upgrades)
                for (let i in entity.upgrades) {
                    let upgradeid = entity.upgrades[i];
                    let upgrade = Types.CHARS[upgradeid];

                    upgrades += '<div class="upgrade" data-entity="' + upgrade.name + '"><span class="cost">(' + upgrade.cost + 'g)</span>' + upgrade.name + '</div>';
                }

            if (playerid == this.myPlayer.playerid)
                template = template.replace('[UPGRADES]', '<div class="upgrades">' + upgrades + '</div>');
            else
                template = template.replace('[UPGRADES]', '');

            let objects = document.querySelector(".objects");
            objects.innerHTML += template;
            item = document.getElementById(msg.uid);

            // upgrades
            if (msg.t == 'char' && playerid == this.myPlayer.playerid)
            {
                let area_defender = document.querySelector('.defender_area[data-c="' + position[0] + ',' + position[1] + '"]');
                // @ts-ignore
                area_defender.innerHTML = item.querySelector('.upgrades').cloneNode(true).outerHTML;

                this.bindAreas();
            }
            */
        }

        //item.style.transitionDuration = msg.s + 'ms';

        // set Position
        if( myEntity )
        {
            const posX = position[0];
            const posY = position[1];

            let offsetX = Math.round(this.mapWidth / 2);
            let offsetY = Math.round(this.mapHeight / 2);

            //myEntity.setPosition(posX - offsetX, posY - offsetY, 0);
            gsap.to(myEntity.getPosition(), { duration: entity.speed / 1000, x: posX - offsetX, z: posY - offsetY, y: 0, ease: "none" });
        }

        if( target )
            myEntity.model.lookAt(target.position)
        myEntity.model.visible = true;

        myEntity.state.element.innerText = mode + ' ' + direction;

        myEntity.lifebar.needsUpdate = true;
        myEntity.lifebar.textContent = msg.l;
        myEntity.lifebar.element.innerText = msg.l;

        if ( myEntity && mode == 'wait' )
            myEntity.idle();
        if ( myEntity && mode == 'walk' )
            myEntity.walk();
        if ( myEntity && mode == 'attack' )
            myEntity.attack();

        myEntity.direction = direction;




        //item.style.width = this.tilesize + "px";
        //item.style.height = this.tilesize + "px";

        // life
        //item.querySelector('.life .bar')["style"].width = parseInt((msg.l / msg.m) * 100) + "%";

        /*
        // castbar
        if (mode == 'cast') {
            item.querySelector('.cast .bar')["style"].transitionDuration = msg.casttime + "ms";
            item.querySelector('.cast .bar')["style"].width = '100%';
        }

        if (mode == 'use') {
            item.querySelector('.cast .bar')["style"].transitionDuration = 0;
            item.querySelector('.cast .bar')["style"].width = "0%";
        }

        // animation
        item.querySelector('sprite').className = 'char-' + mode + '-' + direction;
        item.querySelector('sprite').textContent = position[0] + ', ' + position[1];

        let area = item.querySelector('.area');
        area.style.left = parseInt(position[1] * tilesize) + "px";
        area.style.top = parseInt(position[0] * tilesize) + "px";
        area.style.width = this.tilesize + "px";
        area.style.height = this.tilesize + "px";

        let audio_voice = <HTMLAudioElement> item.querySelector('audio.voice');
        let audio_hit = <HTMLAudioElement> item.querySelector('audio.hit');
        let audio_dead = <HTMLAudioElement> item.querySelector('audio.dead');
        let audio_walk = <HTMLAudioElement> item.querySelector('audio.walk');

        if (type == 'spawn' && audio_voice) {
            this.playAudio(audio_voice, msg.uid);
        }

        if (mode == 'attack' && audio_hit) {
            this.playAudio(audio_hit, msg.uid);
        }

        if (mode == 'walk' && audio_walk) {
            this.playAudio(audio_walk, msg.uid);
        }

        if (mode == 'dead' && audio_dead) {
            this.playAudio(audio_dead, msg.uid);
        }
        */
    }

    public runEffect(effectname: string, position)
    {
        return

        let effect = <HTMLElement> document.querySelector('.effect[data-name="' + effectname + '"]').cloneNode(true);

        document.querySelector('.objects').appendChild(effect);

        effect.style.left = parseInt(position[1] * this.tilesize) + "px";
        effect.style.top = parseInt(position[0] * this.tilesize) + "px";
        effect.style.display = 'block';

        effect.querySelector('sprite').className = 'run'; // start animation

        this.playAudio( effect.querySelector('audio') );

        setTimeout(() =>
        {
            effect.remove();
        }, 3000 );
    }


    public async playAudio(audio: HTMLAudioElement, uid?)
    {
        let cln = <HTMLAudioElement> audio.cloneNode(true);

        cln.currentTime = Number(audio.dataset.start);
        cln.volume = 0.2;

        const playPromise = await cln.play().then(() =>
        {
            // Audio Loading Successful
            // Audio playback takes time
            setTimeout(() => {
                cln.pause();
                cln.currentTime = 0;
            }, parseInt(cln.dataset.duration)); // audio.duration is the length of the audio in seconds.
        });
    }

    private mapWidth = 0;
    private mapHeight = 0;
    private mapTileSize = 16;

    public drawMap(map_json)
    {
        console.log("draw map", map_json);

        let canvas = document.createElement("canvas"),
            ctx = canvas.getContext("2d");

        this.mapHeight = 32
        this.mapWidth = 32
        this.mapTileSize = 32;

        let tilemap_size = this.mapTileSize;
        canvas.height = tilemap_size * map_json.height;
        canvas.width = tilemap_size * map_json.width;

        this.addGround(canvas, this.mapWidth, this.mapHeight);

        this.play();
        return;

        let tileset = new Image();
        tileset.src = '/sprites/map_bg_extended.png';
        tileset.onload = () =>
        {
            // tileset breite und höhe einbeziehen
            let tileset_x = Math.round(tileset.height / tilemap_size);
            let tileset_y = Math.round(tileset.width / tilemap_size);

            for( let i in map_json.layers )
            {
                let layer = map_json.layers[i];
                let layerid = layer.id;

                let zone = null;
                if( layer.properties && layer.properties.filter(obj => {return obj.name == 'zone'})[0] )
                    zone = layer.properties.filter(obj => {return obj.name == 'zone'})[0].value;

                let playerNr = null;
                if( layer.properties && layer.properties.filter(obj => {return obj.name == 'player'})[0] )
                    playerNr = parseInt( layer.properties.filter(obj => {return obj.name == 'player'})[0].value );

                // draw
                if( layer.visible == true && layer.data )
                {
                    for( let j in layer.data )
                    {
                        if( layer.data[j] == 0 )
                            continue;

                        let tileid = layer.data[j] - 1;

                        // locate postion on png tileset
                        let tile_on_tileset_y = Math.floor(tileid / tileset_y);
                        let tile_on_tileset_x = Math.floor(tileid - ( tileset_y * tile_on_tileset_y ) );

                        // target tile on canvas (32x32)
                        let target_on_map_y = Math.floor(j / map_json.width);
                        let target_on_map_x = Math.floor(j - ( map_json.height * target_on_map_y ) );

                        // Create a pattern, offscreen
                        const patternCanvas = document.createElement('canvas');
                        const patternContext = patternCanvas.getContext('2d');

                        // Give the pattern a width and height of 32
                        patternCanvas.width = tilemap_size;
                        patternCanvas.height = tilemap_size;

                        // get tile from tileset map (png)
                        patternContext.drawImage(tileset, tile_on_tileset_x * tilemap_size, tile_on_tileset_y * tilemap_size, tilemap_size, tilemap_size, 0, 0, tilemap_size, tilemap_size);

                        //const pattern = ctx.createPattern(patternCanvas, 'repeat');
                        //ctx.fillStyle = pattern;
                        //ctx.fillRect(y, x, tilesize, tilesize);
                        //ctx.drawImage(patternCanvas, 0, 0, tilemap_size, tilemap_size, target_on_map_x * tilemap_size, target_on_map_y * tilemap_size, tilemap_size, tilemap_size);
                    }
                }
            }

            this.play();
            return;

            this.drawItem({
                n: "spore",
                p: [0, 0],
                t: "monster",
                uid: 1
            }, 'idle', 'char' )

            this.drawItem({
                n: "wolf",
                p: [1, 1],
                t: "monster",
                uid: 4
            }, 'idle', 'char' )

            this.drawItem({
                n: "spider",
                p: [-1, 0],
                t: "monster",
                uid: 2
            }, 'idle', 'char' )

        };

        return;
    }

    private ground: Mesh;

    private addGround(canvas: HTMLCanvasElement, width, height)
    {

        const texture = new THREE.CanvasTexture(canvas)
        texture.magFilter = THREE.NearestFilter;
        texture.minFilter = THREE.LinearMipMapLinearFilter;
        texture.colorSpace = THREE.SRGBColorSpace;

        const material = new THREE.MeshLambertMaterial({
            //color: 0xff0000,
            map: texture,
            //transparent: true,
            opacity: 1,
            //side: THREE.DoubleSide,
            depthWrite: false,
            depthTest: false,
            blending: THREE.AdditiveBlending,

            //emissive: new THREE.Color(0xffffff),
            //emissiveMap: mapTexture
        });

        material.map.minFilter = THREE.LinearFilter;
        //mapMaterial.map.rotation = Math.PI / 2;

        const material2 = new THREE.MeshBasicMaterial( {color: 0x555555} ); // debug

        const geometry = new THREE.PlaneGeometry(width, height);
        const ground = new THREE.Mesh(geometry, material2);

        ground.position.x = 0 // rot
        ground.position.y = 0 // grün
        ground.position.z = -0.5 // blau
        ground.rotation.x = ( Math.PI / 2 ) * -1;
        ground.matrixAutoUpdate = false;
        ground.receiveShadow = true;
        ground.updateMatrix();
        this.ground = ground;

        this.scene.add(ground);

        const cameraFolder = this.gui.addFolder('Camera')
        cameraFolder.add(this.camera.position, 'x')
        cameraFolder.add(this.camera.position, 'y')
        cameraFolder.add(this.camera.position, 'z')
        cameraFolder.add(this.camera.rotation, 'x')
        cameraFolder.add(this.camera.rotation, 'y')
        cameraFolder.add(this.camera.rotation, 'z')
        cameraFolder.open()
    }

    private sound_started = false;

    public makeSound(element)
    {
        if (this.sound_started)
            return false;

        let lines = [
            [22.0, 1500],
            [29.2, 1000],
            [34.0, 1500],
            [2.5, 1000],
            [1.0, 1000],
        ];

        let line = lines[getRandomInt(0, lines.length - 1)];

        //console.log("play sound time: " + line);
        //console.log(lines);

        // random voice wäre geil
        element.currentTime = line[0];
        element.play();
        this.sound_started = true;
        setTimeout(() =>
        {
            element.pause();
            this.sound_started = false;
        }, line[1]);
    }

    public bindAreas()
    {
        let areas = document.querySelectorAll(".defender_area");
        [].forEach.call(areas, (area) =>
        {
            delete area.onclick;

            area.addEventListener("click", (event) =>
            {
                event.preventDefault();

                areas.forEach( e => e.classList.remove("show-upgrades") );
                event.currentTarget.classList.add("show-upgrades");

                return false;
            });

            let upgrades = area.querySelectorAll('.upgrade');

            [].forEach.call(upgrades, (upgrade) =>
            {
                delete upgrade.onclick;

                upgrade.addEventListener("click", (event) =>
                {
                    event.preventDefault();

                    let pos = area.dataset.c.split(',');
                    this.buyDefender( upgrade.dataset.entity, [pos[0],pos[1]] );

                    return false;
                });
            });
        });
    }

    private highlightArea(position)
    {
        let area = document.getElementById("highlighter");
        area.style.top = parseInt(position[0] * this.tilesize) + "px";
        area.style.left = parseInt(position[1] * this.tilesize) + "px";
        area.style.width = this.tilesize + "px";
        area.style.height = this.tilesize + "px";
    }

    private targetArea(position)
    {
        let target = document.getElementById("targeter");
        target.style.top = parseInt(position[0] * this.tilesize) + "px";
        target.style.left = parseInt(position[1] * this.tilesize) + "px";
        target.style.width = this.tilesize + "px";
        target.style.height = this.tilesize + "px";
    }

    public buyDefender(entity, pos)
    {
        if( entity == 'novice' )
            this.socket.emit('buy defender', {entity: entity, pos: pos});
        else
            this.socket.emit("upgrade defender", {entity: entity, pos: pos });
    }

    private zoom = 10;
    private zoomMax = 15;
    private zoomMin = 2;

    public increaseZoom()
    {
        this.zoom++;
        if( this.zoom > this.zoomMax )
            this.zoom = this.zoomMax;

        this.camera.position.setZ(this.zoom)
        this.gui.updateDisplay()
    }

    public decreaseZoom()
    {
        this.zoom--;
        if( this.zoom < this.zoomMin )
            this.zoom = this.zoomMin;

        this.camera.position.setZ(this.zoom)
        this.gui.updateDisplay()
    }

    private is_camera_rotated = false
    private camera_rotate_step = 0.1;
    private camera_rotate = 0;
    private camera_rotate_max = 0.7;

    public rotateCamera()
    {
        this.camera_rotate += this.camera_rotate_step;
        if( this.camera_rotate > this.camera_rotate_max )
            this.camera_rotate = this.camera_rotate_max

        this.increaseZoom();
        this.camera.rotation.x = this.camera_rotate
        this.camera.position.y = this.camera_rotate * 1.5 * -10
        this.gui.updateDisplay()
    }

    public derotateCamera()
    {
        this.camera_rotate -= this.camera_rotate_step;
        if( this.camera_rotate < 0 )
            this.camera_rotate = 0

        this.decreaseZoom();
        this.camera.rotation.x = this.camera_rotate
        this.camera.position.y = this.camera_rotate * 1.5 * -10
        this.gui.updateDisplay()
    }
}