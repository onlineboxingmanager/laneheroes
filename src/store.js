import { writable } from 'svelte/store';

export const myPlayerid = writable(0)
export const myPlayername = writable("")

export const totalPlayers = writable(0)
export const totalLobbys = writable(0)
