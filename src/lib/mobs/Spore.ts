import SpriteTileset from "../../SpriteTileset";
import Game from "../../Game";

export class Spore extends SpriteTileset
{
    constructor(game: Game)
    {
        super('/sprites/mobs/tileset_spore_240x240.png', 8, 8, 64, game);
        this.idle();
    }

    update(delta?: number)
    {
        this.animation_time += delta;
        super.update(delta);
    }

    idle() {
        this.loop([0, 1, 2, 3, 4, 5, 6, 7], 2.5);
    }

    walk() {
        this.loop([16, 17, 18, 19, 20, 21, 22, 23], 2.5);
    }

}