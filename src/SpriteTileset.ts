import {
	Clock,
	LinearFilter,
	LinearMipMapNearestFilter,
	NearestFilter,
	Sprite,
	SpriteMaterial,
	Texture,
	TextureLoader
} from "three";
import Game from "./Game";
import * as THREE from "three";
import * as SkeletonUtils from 'three/addons/utils/SkeletonUtils.js';
import type {GLTF} from "three/examples/jsm/loaders/GLTFLoader";
import {GLTFLoader} from "three/examples/jsm/loaders/GLTFLoader";
import {randInt} from "three/src/math/MathUtils";

export default class SpriteTileset
{

	private position: any;
	private elapsedTime: number;
	protected animation_time: number;
	private clock: Clock;
	private map: Texture;
	sprite: Sprite;
	model: GLTF
	private is_loop: boolean;
	private callback: null;
	private runningTileArrayIndex: number;
	private currentTile: any;
	private playSpriteIndices: any[];
	private maxDisplayTime: number;
	public direction: string;

	private mixer;
	private animationActions: THREE.AnimationAction[] = []
	private last_action;

	/**
	 *
	 * @param spriteTexture A sprite sheet with sprite tiles
	 * @param tilesHoriz Horizontal number of tiles
	 * @param tilesVert Vertical number of tiles
	 */
	constructor(private entity: string,
				private tilesHoriz: number,
				private tilesVert: number,
				private tileSizePixel: number,
				private game: Game)
	{
		this.animation_time = 0;

		this.clock = new Clock();
		this.elapsedTime = 0;

		this.map = new TextureLoader().load(entity.tileset);
		this.map.minFilter = LinearMipMapNearestFilter;   // sharp pixel sprite
		this.map.magFilter = LinearFilter;   // sharp pixel sprite
		this.map.repeat.set(1 / tilesHoriz, 1 / tilesVert);
		this.map.colorSpace = THREE.SRGBColorSpace;

		// debug
		const geometry = new THREE.BoxGeometry( 1, 1, 1 );
		const material2 = new THREE.MeshBasicMaterial( {color: entity.color ?? 0x00ff00} );
		const cube = new THREE.Mesh( geometry, material2 );

		// debug
		//this.model = this.game.models.knight.clone();
		this.model = SkeletonUtils.clone(this.game.models.knight.scene ) ?? cube; // debug
		this.mixer = new THREE.AnimationMixer( this.model );


		//this.sprite = cube; // debug
		//console.log(this.sprite.geometry.computeBoundingBox());
		this.model.geometry?.computeBoundingBox(); // create 3D box (hitboxes for our chars)
		//console.log(this.sprite.geometry);

		this.position = this.model.position;

		this.idle();
	}

	setAnimation(row, start, length, duration)
	{
		let playSpriteIndices = [];

		for( let i = 0; i < length; i++)
		{
			playSpriteIndices.push( ( row * this.tilesHoriz ) + start + i  );
		}

		console.log(playSpriteIndices)
		console.log(duration)

		this.loop(playSpriteIndices, duration);
	}

	loop(playSpriteIndices: any[], totalDuration: number)
	{
		this.is_loop = true;
		this.callback = null;
		this.playSpriteIndices = playSpriteIndices;
		this.runningTileArrayIndex = 0;
		this.currentTile = playSpriteIndices[this.runningTileArrayIndex];
		this.maxDisplayTime = totalDuration / this.playSpriteIndices.length;
		this.elapsedTime = this.maxDisplayTime; // force to play new animation
	}

	animation(playSpriteIndices, totalDuration, callback)
	{
		this.is_loop = false;
		this.callback = callback;
		this.playSpriteIndices = playSpriteIndices;
		this.runningTileArrayIndex = 0;
		this.currentTile = playSpriteIndices[this.runningTileArrayIndex];
		this.maxDisplayTime = totalDuration / this.playSpriteIndices.length;
		this.elapsedTime = this.maxDisplayTime; // force to play new animation
	}

	setPosition(x, y, z) {
		this.model.position.x = x;
		this.model.position.y = y;
		this.model.position.z = z;
	}

	addPosition(x, y, z) {
		this.model.position.x += x;
		this.model.position.y += y;
		this.model.position.z += z;
	}

	getPosition() {
		return this.model.position;
	}

	update(delta: number)
	{
		this.elapsedTime += delta;

		this.mixer?.update(delta)

		if (this.maxDisplayTime > 0 && this.elapsedTime >= this.maxDisplayTime)
		{
			this.elapsedTime = 0;
			this.runningTileArrayIndex = (this.runningTileArrayIndex + 1) % this.playSpriteIndices.length;
			this.currentTile = this.playSpriteIndices[this.runningTileArrayIndex];

			const offsetX = (this.currentTile % this.tilesHoriz) / this.tilesHoriz;
			const offsetY = (this.tilesVert - Math.floor(this.currentTile / this.tilesHoriz) - 1) / this.tilesVert;

			if( this.direction == 'so' )
			{
			}
			// disabled for debug
			//this.map.offset.x = offsetX;
			//this.map.offset.y = offsetY;
		}
	}

	idle()
	{
		const action = this.mixer.clipAction( this.game.models.knight.animations[36] );

		this.last_action?.fadeOut(1);

		action.reset();
		action.fadeIn(1)
		action.play();

		this.last_action = action;
	}

	walk()
	{
		const action = this.mixer.clipAction( this.game.models.knight.animations[randInt(72, 73) ] );

		this.last_action?.fadeOut(1);

		action.reset();
		action.fadeIn(1)
		action.play();

		this.last_action = action;
	}

	attack()
	{
		const action = this.mixer.clipAction( this.game.models.knight.animations[ randInt(0, 3) ] );

		this.last_action?.fadeOut(1);

		action.reset();
		action.fadeIn(1)
		action.play();

		this.last_action = action;
	}
}