Types = {

    Messages: {
        CREATE: 0,
        LOGIN: 1,
        WELCOME: 2,
        SPAWN: 3,
        DESPAWN: 4,
        MOVE: 5,
        LOOTMOVE: 6,
        AGGRO: 7,
        ATTACK: 8,
        HIT: 9,
        HURT: 10,
        HEALTH: 11,
        CHAT: 12,
        LOOT: 13,
        EQUIP: 14,
        DROP: 15,
        TELEPORT: 16,
        DAMAGE: 17,
        POPULATION: 18,
        KILL: 19,
        LIST: 20,
        WHO: 21,
        ZONE: 22,
        DESTROY: 23,
        HP: 24,
        BLINK: 25,
        OPEN: 26,
        CHECK: 27,
        PVP: 28,
        GUILD: 29,
        GUILDERROR: 30,
        GUILDERRORTYPE: {
        	DOESNOTEXIST: 1,
        	BADNAME: 2,
        	ALREADYEXISTS: 3,
        	NOLEAVE: 4,
        	BADINVITE: 5,
        	GUILDRULES: 6,
        	IDWARNING: 7
        },
        GUILDACTION: {
			CONNECT: 8,
			ONLINE: 9,
			DISCONNECT: 10,
			INVITE: 11,
			LEAVE: 12,
			CREATE: 13,
			TALK: 14,
			JOIN: 15,
			POPULATION: 16
		}
    },

    CHARS:
    {

        novice: {
            id: 1,
            name: 'novice',
            life: 100,
            armor: 0,
            evasion: 10,
            speed: 550,
            attack_speed: 1250,
            damage: [7, 10],
            range: 1,
            cost: 100,
	        money: 5,

            audio: {
                voice: ['345048__11linda__female-hero-rpg-request.wav', 22.00, 1500],
                hit: ['effects_hits.m4a', 45.50, 1000],
                //walk: ['char.mp3', 13.00, 2.00],
                //dead: ['char.mp3', 13.00, 2.00],
            },
            tileset: '/character/novice.png',

            upgrades: [
                'mage',
                'sword',
                'archer',
	            'thief',
	            'priest',
            ]

        },

        sword: {
            id: 2,
            name: 'sword',
            life: 450,
            armor: 1,
            evasion: 2,
            speed: 550,
            attack_speed: 1200,
            damage: [10, 20],
            range: 1,
            cost: 125,
	        money: 10,
	        skills: [ 'bash' ],

            audio: {
	            voice: ['345048__11linda__female-hero-rpg-request.wav', 29.20, 1000],
	            hit: ['effects_hits.m4a', 45.50, 1500],
            },
            tileset: '/character/sword.png',

            upgrades: [
                'paladin',
                'knight'
            ]
        },

	    thief: {
		    id: 7,
		    name: 'thief',
		    life: 300,
		    armor: 0,
		    evasion: 25,
		    speed: 1250,
		    attack_speed: 1000,
		    damage: [15, 25],
		    range: 1,
		    cost: 125,
		    money: 10,
		    skills: [],

		    audio: {
			    voice: ['345048__11linda__female-hero-rpg-request.wav', 29.20, 1000],
			    hit: ['effects_hits.m4a', 45.50, 1500],
		    },
		    tileset: '/character/thief.png',

		    upgrades: [
				'rogue',
		    ]
	    },

	    archer: {
		    id: 3,
		    name: 'archer',
		    life: 200,
		    armor: 0,
		    evasion: 5,
		    speed: 1250,
		    attack_speed: 1250,
		    damage: [10, 20],
		    range: 5,
		    cost: 150,
		    money: 10,

		    audio: {
			    voice: ['345048__11linda__female-hero-rpg-request.wav', 29.20, 1000],
			    hit: ['effects_hits.m4a', 45.50, 1500],
		    },
		    tileset: '/character/archer.png',

		    upgrades: [
			    'hunter'
		    ]
	    },

        mage: {
            id: 4,
            name: 'mage',
            life: 200,
            armor: 0,
            evasion: 0,
            speed: 1250,
            attack_speed: 2000,
            damage: [10, 17],
            range: 3,
            cost: 175,
	        money: 10,

	        skills: ['thunder'],

            audio: {
	            voice: ['345048__11linda__female-hero-rpg-request.wav', 34.0, 1500],
            },
            tileset: '/character/mage.png',

            upgrades: [
               'wizard'
            ]
        },

        wizard: {
	        id: 10,
	        name: 'wizard',
	        life: 600,
	        armor: 0,
	        evasion: 0,
	        speed: 1250,
	        attack_speed: 1500,
	        damage: [20, 30],
	        range: 3,
	        cost: 2000,
	        money: 100,
			skills: ['thunder2', 'meteor'],
	        audio: {
		        voice: ['345048__11linda__female-hero-rpg-request.wav', 34.0, 1500],
	        },
	        tileset: '/character/wizard.png',

	        upgrades: []
        },

        knight: {
	        id: 14,
	        name: 'knight',
	        life: 800,
	        armor: 3,
	        evasion: 10,
	        speed: 1250,
	        attack_speed: 1200,
	        damage: [40, 60],
	        range: 1,
	        cost: 2000,
	        money: 100,
	        skills: [],
	        audio: {
		        voice: ['345048__11linda__female-hero-rpg-request.wav', 34.0, 1500],
		        hit: ['effects_hits.m4a', 45.50, 1500],
	        },
	        tileset: '/character/rune_knight.png',

	        upgrades: []
        },

        paladin: {
	        id: 15,
	        name: 'paladin',
	        life: 800,
	        armor: 2,
	        evasion: 0,
	        speed: 1250,
	        attack_speed: 1500,
	        damage: [35, 50],
	        range: 1,
	        cost: 2000,
	        money: 500,
	        skills: [],
	        audio: {
		        voice: ['345048__11linda__female-hero-rpg-request.wav', 34.0, 1500],
		        hit: ['effects_hits.m4a', 45.50, 1500],
	        },
	        tileset: '/character/paladin.png',

	        upgrades: []
        },

        hunter: {
	        id: 13,
	        name: 'hunter',
	        life: 400,
	        armor: 0,
	        evasion: 15,
	        speed: 1250,
	        attack_speed: 1000,
	        damage: [30, 45],
	        range: 5,
	        cost: 2000,
	        money: 500,

	        audio: {
		        voice: ['345048__11linda__female-hero-rpg-request.wav', 29.20, 1000],
		        hit: ['effects_hits.m4a', 45.50, 1500],
	        },
	        tileset: '/character/hunter.png',

	        upgrades: []
        },

        rogue: {
	        id: 12,
	        name: 'rogue',
	        life: 500,
	        armor: 0,
	        evasion: 40,
	        speed: 1250,
	        attack_speed: 800,
	        damage: [35, 50],
	        range: 1,
	        cost: 2000,
	        money: 100,
	        skills: [],

	        audio: {
		        voice: ['345048__11linda__female-hero-rpg-request.wav', 29.20, 1000],
		        hit: ['effects_hits.m4a', 45.50, 1500],
	        },
	        tileset: '/character/rogue.png',

	        upgrades: []
        },

	    priest: {
		    id: 10,
		    name: 'priest',
		    life: 500,
		    armor: 0,
		    evasion: 0,
		    speed: 1250,
		    attack_speed: 1500,
		    damage: [15, 20],
		    range: 1,
		    cost: 2, // 2000
		    money: 100,
		    skills: [
		    	'heal'
		    ],

		    audio: {
			    voice: ['345048__11linda__female-hero-rpg-request.wav', 29.20, 1000],
			    hit: ['effects_hits.m4a', 45.50, 1500],
		    },
		    tileset: '/character/priest.png',

		    upgrades: []
	    },

        king: {
            id: 9999,
            name: 'king',
            life: 10000,
            armor: 5,
            evasion: 10,
            speed: 0,
            attack_speed: 1000,
            damage: [50, 100],
            range: 1,

            audio: {
	            hit: ['effects_hits.m4a', 45.50, 1500],
            },
            tileset: '/character/king.png',
            upgrades: [
            ]
        },

	    king2: {
		    id: 9999,
		    name: 'king2',
		    life: 10000,
		    armor: 5,
		    evasion: 10,
		    speed: 0,
		    attack_speed: 1000,
		    damage: [50, 100],
		    range: 1,

		    audio: {
			    hit: ['effects_hits.m4a', 45.50, 1500],
		    },
		    tileset: '/character/king2.png',
		    upgrades: [
		    ]
	    },

	    king3: {
		    id: 9999,
		    name: 'king3',
		    life: 10000,
		    armor: 5,
		    evasion: 10,
		    speed: 0,
		    attack_speed: 1000,
		    damage: [50, 100],
		    range: 1,

		    audio: {
			    hit: ['effects_hits.m4a', 45.50, 1500],
		    },
		    tileset: '/character/king3.png',
		    upgrades: [
		    ]
	    },

    },

    MOBS: {

	    spore: {
		    id: 1,
		    name: 'spore',
		    life: 35,
		    armor: 0,
		    evasion: 0,
		    speed: 450,
		    attack_speed: 1250,
		    damage: [2, 4],
		    range: 1,
		    money: 10,
		    is_flying: false,
		    audio: {
			    hit: ['spore_attack.wav', 0.00, 1200],
			    walk: ['spore_move2.wav', 0.00, 1200],
			    dead: ['spore_die.wav', 0.00, 2500],
		    },
		    tileset: '/mobs/tileset_spore_240x240.png'
	    },

        spider: {
            id: 2,
            name: 'spider',
            life: 100,
            armor: 0,
            evasion: 15,
            speed: 1250,
            attack_speed: 1200,
	        damage: [7, 10],
            range: 1,
            money: 20,
            is_flying: false,
            audio: {
                hit: ['argos_attack.wav', 0.00, 1.50],
                walk: ['argos_move.wav', 0.00, 1.50],
                dead: ['monster_insect.wav', 0.00, 1.50],
            },
            tileset: '/mobs/tileset_spider_240x240.png'
        },

	    crab: {
		    id: 3,
		    name: 'crab',
		    life: 200,
		    armor: 1,
		    evasion: 0,
		    speed: 1500,
		    attack_speed: 1500,
		    damage: [13, 18],
		    range: 1,
		    money: 25,
		    is_flying: false,
		    audio: {
			    voice: ['crab_stand.wav', 0.00, 1.50],
			    hit: ['crab_attack.wav', 0.00, 1.50],
			    walk: ['crab_stand.wav', 0.00, 1.50],
			    dead: ['crab_die.wav', 0.00, 1.50],
		    },
		    tileset: '/mobs/tileset_crab_240x240.png'
	    },

	    wolf: {
		    id: 4,
		    name: 'wolf',
		    life: 60,
		    armor: 0,
		    evasion: 10,
		    speed: 1100,
		    attack_speed: 1200,
		    damage: [13, 16],
		    range: 1,
		    money: 25,
		    is_flying: false,
		    audio: {
			    voice: ['wolf_stand.wav', 0.00, 1.50],
			    hit: ['wolf_attack.wav', 0.00, 1.50],
			    walk: ['wolf_walk.wav', 0.00, 1.50],
			    dead: ['wolf_die.wav', 0.00, 2.50],
		    },
		    tileset: '/mobs/tileset_wolf_240x240.png'
	    },

	    orc: {
		    id: 5,
		    name: 'orc',
		    life: 250,
		    armor: 1,
		    evasion: 5,
		    speed: 1500,
		    attack_speed: 2000,
		    damage: [20, 30],
		    range: 1,
		    is_flying: false,
		    money: 50,
		    audio: {
			    voice: ['ork_warrior_breath.wav', 0.00, 3.00],
			    hit: ['ork_warrior_attack1.wav', 0.00, 1.00],
			    walk: ['ork_warrior_step2.wav', 0.00, 1.00 ],
			    dead: ['ork_warrior_die2.wav', 0.00, 2.00],
		    },
		    tileset: '/mobs/tileset_orc_240x240.png'
	    },

	    boss_orchero: {
		    id: 60,
		    name: 'boss_orchero',
		    life: 1000,
		    armor: 2,
		    evasion: 0,
		    speed: 1000,
		    attack_speed: 1500,
		    damage: [50, 75],
		    range: 1,
		    is_flying: false,
		    money: 1500,
		    tileset: '/mobs/tileset_boss_orchero_240x240.png'
	    },

        icewolf: {
            id: 30,
            name: 'icewolf',
            life: 300,
            armor: 1,
            evasion: 15,
            speed: 1000,
            attack_speed: 1500,
	        damage: [20, 30],
            range: 1,
            is_flying: false,
            money: 500,
            audio: {
                voice: ['wolf_stand.wav', 0.00, 1.20],
                hit: ['wolf_attack.wav', 0.00, 1.20],
                walk: ['were_wolf_foot1.wav', 0.00, 1.00 ],
                dead: ['wolf_die.wav', 0.00, 2.00],
            },
            tileset: '/mobs/tileset_icewolf_240x240.png'
        },

        mummy: {
            id: 40,
            name: 'mummy',
            life: 500,
            armor: 0,
            evasion: 0,
            speed: 1500,
            attack_speed: 2000,
            damage: [30, 40],
            range: 1,
            money: 250,
            is_flying: false,
            tileset: '/mobs/tileset_mummy_240x240.png',
	        audio: {
		        voice: ['mummy_move1.wav', 0.00, 2.00 ],
		        hit: ['mummy_attack.wav', 0.00, 1.20],
		        walk: ['mummy_move1.wav', 0.00, 2.00 ],
		        dead: ['mummy_die.wav', 0.00, 2.00],
	        },
        },

        boss_chimera: {
            id: 50,
            name: 'boss_chimera',
            life: 1000,
            armor: 2,
            evasion: 10,
            speed: 1250,
            attack_speed: 1500,
            damage: [20, 25],
            range: 1,
            is_flying: false,
            money: 2500,
            tileset: '/mobs/tileset_boss_chimera_240x240.png',
	        audio: {
		        voice: ['chimera_damage.wav', 0.00, 2.00],
		        hit: ['chimera_damage.wav', 0.00, 2.00],
		        walk: ['minorous_move.wav', 0.000, 1.50 ],
		        dead: ['chimera_die.wav', 0.00, 2.00],
	        },
        },



    },

	SKILLS:
	{
		bash: {
			name: 'bash',
			type: 'damage',
			damage: [20,25],
			range: 1,
			radius: 0,
			casttime: 0,
			cooldown: 10000,
			audio: ['skill_bash.m4a', 0.50, 2000],
			tileset: ['/effects/tileset_bash_240x240.png', 15, 2250],
		},

		heal: {
			type: 'heal',
			name: 'heal',
			damage: [150, 250],
			range: 3,
			radius: 0,
			casttime: 2000,
			cooldown: 10000,
			audio: ['skill_bless.m4a', 0.50, 3000],
			//tileset: ['/effects/tileset_bash_240x240.png', 15, 2250],
		},

		thunder: {
			name: 'thunder',
			type: 'damage',
			damage: [50, 100],
			range: 4,
			radius: 1,
			casttime: 2500,
			cooldown: 20000,
			audio: ['skill_thunder.m4a', 0.50, 2000],
			tileset: ['/effects/tileset_thunder_240x240.png', 15, 2250],
		},

		thunder2: {
			name: 'thunder2',
			type: 'damage',
			damage: [150, 250],
			range: 4,
			radius: 1,
			casttime: 2500,
			cooldown: 20000,
			audio: ['skill_thunder.m4a', 0.50, 2000],
			tileset: ['/effects/tileset_thunder_240x240.png', 15, 2250],
		},

		meteor: {
			name: 'meteor',
			type: 'damage',
			damage: [300, 350],
			range: 4,
			radius: 1,
			casttime: 2500,
			cooldown: 20000,
			audio: ['skill_thunder.m4a', 0.50, 2000],
			tileset: ['/effects/tileset_thunder_240x240.png', 15, 2250],
		},
	},

};


if(!(typeof exports === 'undefined')) {
    module.exports = Types;
}