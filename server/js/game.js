let Utils = require('./utils');
let Types = require('./gametypes');
let Waves = require('./waves');
let PF = require('pathfinding');
let path = require('path');

let BaseClass = require('./lib/class');
let Player = require('./player.js');
let Monster = require('./monster.js');
let Character = require('./character.js');
let Emperium = require('./emperium.js');
let JsonMap = require('../maps/map_16.json');

class Game extends BaseClass
{
	constructor(worldserver, players, lobbyid)
	{
		super();

		let self = this;

		this.modus = '1vs1';
		this.lobbyid = lobbyid;

		this.worldserver = worldserver;
		this.round = 0;
		this.max_rounds = 30;
		this.time = 0;
		this.players = players;

		this.round_duration = 30 * 1000; // 30 sec
		this.round_duration = 1000; // 10 sec
		this.timer = null;

		this.teams = {};
		this.teams[1] = {};
		this.teams[2] = {};

		this.monsters = [];
		this.chars = [];

		this.is_buyable = false;

		this.map();
		this.start();

		// player can buy some defenders
		for( let i in this.players )
		{
			let player = this.players[i];
			player.game = this;

			this.players[i].playerid = i;

			// send game start
			player.socket.emit('game start', {playerid: parseInt(i) + 1, playername: player.playername, clientid: player.socket.client.id } );

			// bind action buy
			player.socket.on('buy defender', (msg) =>
			{
				self.buy(player, msg.pos, msg.entity);
			});

			// bind action upgrade
			player.socket.on('upgrade defender', (msg) =>
			{
				self.upgrade(player, msg.pos, msg.entity);
			});

			// bind action sell
			player.socket.on('sell defender', (msg) =>
			{

			});
		}
	};

	buy(player, pos, type)
	{
		for( let i in player.chars )
		{
			if( player.chars[i].position.toString() === pos.toString() )
				return false;
		}

		let char = new Character(this, player, pos, type );

		if( player.money < char.cost || this.is_buyable === false )
			return false;

		// set costs
		player.money -= parseInt(char.cost);
		this.sendSockets('player status', player.getState() );

		// spawn
		char.spawn();

		// add to player chars
		this.chars.push(char);
		player.chars.push(char);
	};

	upgrade(player, position, entity)
	{
		let defender = Types.CHARS[entity];
		if( defender.cost > player.money )
			return false;

		// remove old or overwrite ?
		for( let i in player.chars )
		{
			if( player.chars[i].pos_org[0] === position[0] && player.chars[i].pos_org[1] === position[1] )
			{
				player.chars[i].despawn();
				clearTimeout(player.chars[i].timer);
				delete player.chars[i];
			}
		}

		this.buy(player, position, entity);
	};

	map()
	{
		const clients = Object.keys(this.worldserver.clients);

		// create matrix (16 to 32 grid)
		this.grid = new PF.Grid( Math.round(JsonMap.width / 2), Math.round(JsonMap.height / 2) );

		// set all fieds to non-walkable
		for (let y = 0; y < this.grid.height; ++y)
		{
			for (let x = 0; x < this.grid.width; ++x)
			{
				this.grid.setWalkableAt(y, x, false);
			}
		}

		for( let i in JsonMap.layers )
		{
			let layer = JsonMap.layers[i];

			let is_path = false;
			let zone = null;
			let playerNr = null;

			// LAYER properties
			if ( layer.properties )
			{
				for( let o in layer.properties )
				{

					if( layer.properties[o].name === 'is_path' )
						is_path = layer.properties[o].value;

					if( layer.properties[o].name === 'zone' )
						zone = layer.properties[o].value;

					if( layer.properties[o].name === 'player' )
						playerNr = parseInt(layer.properties[o].value) - 1;
				}
			}

			// set walkable paths
			if ( is_path )
			{
				for( let j in layer.data )
				{
					let data = layer.data[j];

					let coord_y = Math.floor( j / layer.width );
					let coord_x = Math.floor( j - ( coord_y * layer.width ) );

					let is_walkable = ( data > 0 );

					let coord = Utils.coordQuad([coord_y, coord_x]);

					if( is_walkable )
						this.grid.setWalkableAt(coord[0], coord[1], true );
				}
			}

			// add king position
			if( layer.name == "King" )
			{
				for(let k in layer.objects )
				{
					let object = layer.objects[k];
					let coord = Utils.coordQuad([Math.floor(object.y / 16), Math.floor(object.x / 16)]);

					playerNr = parseInt( object.properties.filter(obj => {return obj.name === 'player'})[0].value ) - 1;

					if( this.players[playerNr] )
						this.players[playerNr].emperium = new Emperium(this, this.players[playerNr], [ coord[0], coord[1]]);
				}
			}

			// positions (defenders, mob starts, versus start points)
			if ( zone )
			{
				for( let k in layer.objects )
				{
					let object = layer.objects[k];

					let coord_y = Math.floor( object.y / object.width );
					let coord_x = Math.floor( object.x / object.height );

					//let coord = Utils.coordQuad([coord_y, coord_x]);

					if( this.players[playerNr] )
						this.players[playerNr].zones[zone][parseInt(object.name)] = [coord_y, coord_x];
				}
			}
		}

		for( let i in this.players )
		{
			this.sendMap(this.players[i]);
		}
	};

	sendMap(player)
	{
		if( !player.socket.client.id )
			return false;

		console.log("send map to " + player.socket.client.id );
		//player.socket.emit('map show', JsonMap);
		player.socket.emit('map show', '');
	}

	start()
	{
		this.nextRound();
	};

	startRound()
	{
		this.sendSockets('round start', this.round);
		this.sendSockets('set timer', this.round_duration);

		let self = this;

		this.is_buyable = true;

		clearTimeout(this.timer);
		this.timer = setTimeout(() =>
		{
			this.is_buyable = false;

			// fight versus player after preparing time
			if( !Waves[self.round] || Waves[self.round].length === 0 )
				return self.startVersus();
			else
				// send monster wave after preparing time
				self.startWave();

		}, this.round_duration);

		// player money
		for( let i in this.players )
		{
			this.players[i].money += 50 + parseInt( this.round * 5 );
			this.sendSockets('player status', this.players[i].getState() );

			if( this.players[i].emperium )
			{
			this.players[i].emperium.stopTimer();
			this.players[i].emperium.spawn();
			}

			// defender start with full life
			for( let x in this.players[i].chars )
			{
				this.players[i].chars[x].reset();
				this.players[i].chars[x].spawn();
			}

			// do bot things ...
			this.players[i].doBot();
		}

		console.log(":: start round preparation time");
	};

	nextRound()
	{
		let self = this;
		clearTimeout(this.timer);

		this.round++;
		if( this.round >= this.max_rounds )
			return this.end();

		this.timer = setTimeout(function()
		{
			self.startRound();
		}, 5000);
	};

	startWave()
	{
		this.sendSockets('wave start');

		// add monsters for each teams and lane (player)
		this.addMonster();

		for( let i in this.players )
		{
			this.players[i].emperium.attackNearby();

			// defender start attack
			for( let j in this.players[i].chars )
			{
				this.players[i].chars[j].attackNearby();
			}

			// spawn monster and target defenders
			for( let j in this.players[i].monsters )
			{
				this.players[i].monsters[j].spawn();
				this.players[i].monsters[j].targetNPC();
			}
		}

		this.checkWave();
	};

	/**
	 * @returns {*|void}
	 */
	checkWave()
	{
		clearTimeout(this.timer);

		let c = 0;
		for( let x in this.players )
			for( let i in this.players[x].monsters )
				if( this.players[x].monsters[i].life > 0 )
					c++;

		//console.log("check wave .. monster left: " + c + " - total:" + this.monsters.length);

		if( c === 0 )
			return this.nextRound();

		let self = this;
		this.timer = setTimeout(function()
		{
			self.checkWave();
		}, 1000); // alle 3 sek. prüfen
	};

	checkVersus()
	{
		clearTimeout(this.timer);

		let is_end = false;
		for( let x in this.players )
		{
			let c = 0;
			for( let i in this.players[x].monsters )
			{
				if( this.players[x].monsters[i].life > 0 )
					c++;
			}

			if( c === 0 )
			{
				is_end = true;
				// gewinner bekommt extra geld ??
				// oder geld für Defender töten ?
			}
		}

		if( is_end )
		{

			for( let i in this.players )
			{
				for( let m in this.players[i].monsters )
				{
					clearTimeout( this.players[i].monsters[m].timer );
				}

				this.players[i].monsters = [];
			}

			return this.nextRound();
		}

		let self = this;
		this.timer = setTimeout(function()
		{
			self.checkVersus();
		}, 1000); // alle 3 sek. prüfen
	};

	end()
	{
		this.sendSockets('game end');
		console.log("game end - delete all timers");

		clearTimeout(this.timer);

		// game is over
		for( let i in this.players )
		{
			if(this.players[i].emperium)
			clearTimeout(this.players[i].emperium.timer);

			delete this.players[i].monster;
			delete this.players[i].chars;
			delete this.players[i].emperium;
			delete this.players[i];
		}

		for( let i in this.monsters )
		{
			clearTimeout(this.monsters[i].timer);
			delete this.monsters[i];
		}

		for( let i in this.chars )
		{
			clearTimeout(this.chars[i].timer);
			delete this.chars[i];
		}
	};

	sendStatus()
	{
		// send current game status
	};

	addMonster()
	{
		// liste in json auslagern...
		let self = this;

		for( let j in this.players )
		{
			delete this.players[j].monsters;
			this.players[j].monsters = [];
		}

		for( let i in Waves[this.round] )
		{
			let mob = Waves[this.round][i][1]; // später hier mehr ...
			let mob_pos = Waves[this.round][i][0]; // später hier mehr ...

			for( let j in this.players )
			{
				let zones = self.players[j].zones;

				//let random_start = zones.mobs[Math.floor(Math.random() * zones.mobs.length)];
				let random_start = zones.mobs[mob_pos];

				//console.log("random on " + random_start);

				let newmob = new Monster(self, self.players[j], [random_start[0], random_start[1]]);

				newmob.id = mob.id;
				newmob.name = mob.name;
				newmob.speed = mob.speed;
				newmob.attack_speed = mob.attack_speed;
				newmob.life = mob.life;
				newmob.maxlife = mob.life;
				newmob.damage = mob.damage;
				newmob.is_flying = mob.is_flying;
				newmob.armor = mob.armor;
				newmob.evasion = mob.evasion;

				self.monsters.push( newmob );
				self.players[j].monsters.push( newmob );
			}
		}
	};

	startVersus()
	{
		this.sendSockets('wave start');
		this.sendSockets('start versus');

		for( let j in this.players )
		{
			for( let x in this.players[j].chars )
			{
				let char = this.players[j].chars[x];

				// index position suchen
				let index = 0;
				for( let z in this.players[j].zones.defender )
				{
					let def = this.players[j].zones.defender[z];
					if( def.toString() === char.position.toString() )
						index = z;
				}

				char.attackNearby();

				// equalänte versus position (stimmt auf map überein)
				console.log("-----" + index);
				console.log(this.players[j].zones);
				let versus_pos = this.players[j].zones.versus[index];

				// set pos on battle field
				char.position = versus_pos;
			}
		}

		// set defender as enemies on each player
		if( this.players.length === 2 )
		{
			this.players[0].monsters = this.players[1].chars;
			this.players[1].monsters = this.players[0].chars;
		}

		// spawn enemies and target defenders
		for( let j in this.players )
		{
			for( let m in this.players[j].monsters )
			{
				this.players[j].monsters[m].spawn();
				this.players[j].monsters[m].targetNPC();
			}
		}

		this.checkVersus();
	};

	sendSockets(action, value)
	{
		this.worldserver.io.sockets.to(this.lobbyid).emit(action, value);
	};
}

module.exports = Game;