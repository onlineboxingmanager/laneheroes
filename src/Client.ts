import {io, Socket} from "socket.io-client";

export default class Client
{
    public lobbyid: number;
    public socket: Socket;

    public disabled: boolean = false;

    public gameid: HTMLInputElement;

    public messages: HTMLElement;
    private lobby: HTMLElement;
    private clients: HTMLElement;
    private game_start: HTMLElement;
    private game_over: HTMLElement;

    constructor()
    {
        this.lobbyid = parseInt(window.location.search.replace('?', ''));
        this.messages = document.getElementById('messages');
        this.game_start = document.getElementById('game_start');
        this.game_over = document.getElementById('game_over');
        this.gameid = <HTMLInputElement> document.getElementById('gameid');
        this.lobby = document.getElementById('lobby');
        this.clients = document.getElementById('clients');

        // @ts-ignore
        this.socket = io("ws://localhost:3000/", {
            reconnectionDelayMax: 10000,
            query: {
                "lobbyid": this.lobbyid
            },
        });

        // either by directly modifying the `auth` attribute
        this.socket.on("connect_error", (msg) => {
            console.error("connect error " + msg);
        });

        this.socket.on("connection", (reason) => {
            console.error("connect by " + reason);
        });

        this.socket.on("disconnect", (reason) => {
            console.error("disconnect by " + reason);
        });

        this.socket.on('lobby created', (msg) => {
            this.gameid.value = window.location.origin + '/?' + msg;
            this.lobbyid = msg;
            window.history.replaceState({}, '', this.gameid.value);
        });

        this.socket.on('lobby joined',  (msg) => {
            this.gameid.value = window.location.origin + '/?' + msg;
            this.lobbyid = msg;
            window.history.replaceState({}, '', this.gameid.value);
        });

        this.socket.on('total players', (msg) =>
        {
            document.getElementById('players_total').innerText = String(parseInt(msg));
        });

        this.socket.on('total lobbies', (msg) => {
            document.getElementById('lobbies_total').innerText = String(parseInt(msg));
        });

        this.socket.on('chat message', (msg) => {
            let item = document.createElement('li');
            item.textContent = msg;

            this.messages.appendChild(item);
            window.scrollTo(0, document.body.scrollHeight);
        });

        this.socket.on('client list', (list) =>
        {
            this.lobby.style.display = "block";
            this.game_start.style.display = "none";

            document.querySelectorAll('#clients li').forEach(e => e.remove());

            for( let i in list )
            {
                let item = document.createElement('li');
                item.innerText = list[i].playername;
                this.clients.appendChild(item);
            }

            let single = <HTMLInputElement> document.getElementById('singleplayer');
            single.disabled = list.length != 1;

            let multi = <HTMLInputElement> document.getElementById('multiplayer');
            multi.disabled = list.length <= 1;

        });

        this.socket.on("redirect to", function(url)
        {
            return window.location.href = url + '#debug';
        });

        this.binds();

        this.checkLobby();
    }

    public binds()
    {
        document.getElementById('singleplayer').addEventListener("click", () =>
        {
            this.socket.emit('start game', "singleplayer");
            return false;
        });

        document.getElementById('multiplayer').addEventListener("click", () =>
        {
            if( this.disabled )
                return false;

            this.socket.emit('start game', "multiplayer");
            return false;
        });

        let form = <HTMLFormElement> document.getElementById('form');
        let input = <HTMLInputElement> document.getElementById('input');
        form.addEventListener('submit',  (e) =>
        {
            e.preventDefault();

            if (input.value) {
                this.socket.emit('chat message', input.value);
                input.value = '';
            }
        });

        let login = <HTMLFormElement> document.getElementById('login');
        let input_playername = <HTMLInputElement> document.getElementById('input_playername');

        login.addEventListener('submit', (e) =>
        {
            e.preventDefault();

            if ( input_playername.value.length == 0 )
                return false;

            if ( input_playername.value )
                this.socket.emit('create lobby', { playername: input_playername.value });

            document.cookie = "playername=" + input_playername.value;

            return false;
        });

        document.querySelectorAll('#login').forEach(e =>
        {
            e.addEventListener("mouseover", function()
            {
                let spore = document.getElementById("orchero");
                spore.className = spore.className.replace('wait-s', 'attack-n');
            });

            e.addEventListener("mouseout", function()
            {
                let spore = document.getElementById("orchero");
                spore.className = spore.className.replace('attack-n', 'wait-s');
            })
        });

        document.querySelectorAll('#login input').forEach(e =>
        {
            e.addEventListener("focus", function()
            {
                let spore = document.getElementById("mummy");
                spore.className = spore.className.replace('wait-so', 'dead-o');
            });

            e.addEventListener("blur", function()
            {
                let spore = document.getElementById("mummy");
                spore.className = spore.className.replace('dead-o', 'wait-so');
            })
        });

        document.querySelectorAll('#game_start h1').forEach(e =>
        {
            e.addEventListener("mouseover", function()
            {
                let spore = document.getElementById("spore");
                spore.className = spore.className.replace('wait', 'dead');
            });

            e.addEventListener("mouseout", function()
            {
                let spore = document.getElementById("spore");
                spore.className = spore.className.replace('dead', 'wait');
            })
        });

    }

    public checkLobby()
    {
        if( this.lobbyid )
            this.socket.emit("check lobby", this.lobbyid);
    }

}

export function getCookie(cname)
{
    let name = cname + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for(let i = 0; i <ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}


export function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
}

export function coordQuad(coord, factor)
{
    if( !factor )
        factor = 2;

    let y = Math.floor(coord[0] / factor);
    let x = Math.floor(coord[1] / factor);

    //console.log("quadration");
    //console.log(coord);
    //console.log([y,x]);

    return [y,x];
};