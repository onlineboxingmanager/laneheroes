let Entity = require('./entity');
let Messages = require('./message');
let Utils = require('./utils');
let PF = require('pathfinding');
let path = require('path');

class Character extends Entity
{

	constructor(game, player, position, type)
	{
		super();

		this.type = 'char';
		this.game = game;
		this.team = player.team;
		this.player = player;
		this.position = position;
		this.pos_org = [].concat(position);

		let entity = Types.CHARS[type];
		this.name = entity.name;
		this.id = entity.id;
		this.life = entity.life;
		this.maxlife = this.life;
		this.armor = entity.armor;
		this.range = entity.range;
		this.evasion = entity.evasion;
		this.speed = entity.speed;
		this.attack_speed = entity.attack_speed;
		this.damage = entity.damage;
		this.money = entity.money;
		this.cost = entity.cost;

		// add skills
		if( entity.skills )
		{
			for( let i in entity.skills )
			{
				let skill = Object.assign({}, Types.SKILLS[entity.skills[i]]); // clone object
				this.skills[entity.skills[i]] = skill;
				this.skills[entity.skills[i]].is_cd = false;
			}
		}
	};

	attackNearby()
	{
		let self = this;

		console.log("defender scan for targets");

		clearTimeout(this.timer);
		this.timer = setTimeout(function()
		{
			let targets = [];

			let distance = 0;
			for( let i in self.player.monsters )
			{
				let monster = self.player.monsters[i];
				if( monster.life <= 0 )
					continue;

				distance = Utils.distanceTo( monster.position[0], monster.position[1], self.position[0], self.position[1] );
				targets[distance] = monster;

				// debug
				console.log("target monster distance: " + distance + " from player " + monster.player.playerid);
			}

			// attack first monster
			if( targets.length > 0 )
			{
				// sort by distance
				targets.sort((a, b) => a - b);

				self.walkTo(targets[0], function()
				{
					self.attack(targets[0], function()
					{
						self.attackNearby();
					});
				});
			}

			// scan for others
			else
				self.attackNearby();

		}, this.attack_speed);
	}

	targetNPC()
	{
		this.attackNearby();
	}

}

module.exports = Character;