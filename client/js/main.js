let lobbyid = window.location.search.replace('?', '');

let socket = io({
	query: "lobbyid=" + lobbyid
});
// either by directly modifying the `auth` attribute
socket.on("connect_error", (msg) => {
	alert("connect error " + msg);
});

socket.on("connection", (reason) => {
	alert("connect by " + reason);
});

socket.on("disconnect", (reason) => {
	alert("disconnect by " + reason);
});

let map = document.getElementById('map');
let camera = document.getElementById('camera');
let messages = document.getElementById('messages');
let form = document.getElementById('form');
let input = document.getElementById('input');
let login = document.getElementById('login');
let input_playername = document.getElementById('input_playername');
let lobby = document.getElementById('lobby');
let clients = document.getElementById('clients');
let game_start = document.getElementById('game_start');
let game_over = document.getElementById('game_over');
let gameid = document.getElementById('gameid');

setTimeout(function()
{
	let audio_background = document.getElementById("audio_background");
	document.onmouseover = () => audio_background.play();

}, 1000);

document.querySelector('.switch').addEventListener("click", function()
{
	let className = 'player-1';
	if( camera.className == 'player-1' )
		className = 'player-2';

	camera.className = className;

	return false;
});

document.querySelector('.fullscreen').addEventListener("click", function()
{

	camera.className = '';
	camera.style.width = '1024px';

	return false;
});

let myPlayername = getCookie("playername");
input_playername.value = myPlayername;

document.getElementById('singleplayer').addEventListener("click", function()
{
	socket.emit('start game', "singleplayer");
	return false;
});

document.getElementById('multiplayer').addEventListener("click", function()
{
	if( this.disabled )
		return false;

	socket.emit('start game', "multiplayer");
	return false;
});

form.addEventListener('submit', function (e) {
	e.preventDefault();
	if (input.value) {
		socket.emit('chat message', input.value);
		input.value = '';
	}
});

login.addEventListener('submit', function (e)
{
	e.preventDefault();

	if ( input_playername.value.length == 0 )
		return false;

	if ( input_playername.value )
		socket.emit('create lobby', { playername: input_playername.value });

	document.cookie = "playername=" + input_playername.value;

	return false;
});

if( lobbyid )
{
	socket.emit("check lobby", lobbyid);
}

socket.on('lobby created', function (msg) {
	gameid.value = window.location.origin + '/?' + msg;
	lobbyid = msg;
	window.history.replaceState({}, '', gameid.value);
});

socket.on('lobby joined', function (msg) {
	gameid.value = window.location.origin + '/?' + msg;
	lobbyid = msg;
	window.history.replaceState({}, '', gameid.value);
});

socket.on('total players', function (msg) {
	document.getElementById('players_total').innerText = parseInt(msg);
});

socket.on('total lobbies', function (msg) {
	document.getElementById('lobbies_total').innerText = parseInt(msg);
});

socket.on('chat message', function (msg) {
	let item = document.createElement('li');
	item.textContent = msg;

	messages.appendChild(item);
	window.scrollTo(0, document.body.scrollHeight);
});

socket.on('client list', function (list)
{
	lobby.style.display = "block";
	game_start.style.display = "none";

	document.querySelectorAll('#clients li').forEach(e => e.remove());

	for( let i in list )
	{
		let item = document.createElement('li');
		item.innerText = list[i].playername;
		clients.appendChild(item);
	}

	let single = document.getElementById('singleplayer');

	single.disabled = true;
	if( list.length == 1 )
		single.disabled = false;

	let multi = document.getElementById('multiplayer');

	multi.disabled = true;
	if( list.length > 1 )
		multi.disabled = false;

});

let tilesize = 32;
let grid = 32;
let map_size = [64, 64];

let myPlayer;

socket.on("redirect to", function(url)
{
	return window.location.href = url;
});

socket.on("map show", function(json)
{
	drawMap(json);

	camera.style.display = "block";
	lobby.style.display = "none";
	game_start.style.display = "none";
});

socket.on('game start', function (player)
{
	lobby.style.display = "none";
	game_start.style.display = "none";

	document.querySelectorAll('.monster').forEach(e => e.remove());
	document.querySelectorAll('.emperium').forEach(e => e.remove());
	document.querySelectorAll('.char').forEach(e => e.remove());
	document.querySelectorAll('.area_defender').forEach(e => e.remove());

	document.getElementById("player_" + player.playerid ).querySelector('.playername').innerText = player.playername;
	document.getElementById("player_" + player.playerid ).querySelector('.playerid').innerText = player.playerid;

	myPlayer = player;

	camera.className = "player-" + player.playerid;

});

socket.on('game over', function (player)
{
	lobby.style.display = "none";
	camera.style.display = "none";
	game_start.style.display = "none";
	game_over.style.display = "block";

	document.querySelectorAll('.monster').forEach(e => e.remove());
	document.querySelectorAll('.emperium').forEach(e => e.remove());
	document.querySelectorAll('.char').forEach(e => e.remove());
	document.querySelectorAll('.area_defender').forEach(e => e.remove());
});

socket.on('round start', function (msg)
{
	document.querySelectorAll('.phase').forEach( e => e.innerText = 'Vorbereitung');
	document.querySelectorAll('.round').forEach( e => e.innerText = msg);
	document.getElementById('game').className = 'preperation';

	document.querySelectorAll('.show-upgrades').forEach(e => e.classList.remove("show-upgrades")); // close popups
});

socket.on('wave start', function (msg)
{
	document.querySelectorAll('.phase').forEach( e => e.innerText = 'WAVE');
	document.querySelectorAll('.time').forEach( e => e.innerText = '');

	document.getElementById('game').className = 'wave';

	document.querySelectorAll('.show-upgrades').forEach(e => e.classList.remove("show-upgrades")); // close popups
});

socket.on('start versus', function (msg)
{
	document.querySelectorAll('.phase').forEach( e => e.innerText = 'VERSUS');
	document.getElementById('game').className = 'versus';
});

socket.on('player status', function (player)
{
	let id = parseInt(player.playerid) + 1;
	let item = document.getElementById('player_' + id);

	item.querySelector('.money').innerText = parseInt(player.money);
	item.querySelector('.playername').innerText = player.playername;
});

socket.on('set timer', function (msg)
{
	let duration = parseInt(msg);

	let interval = setInterval(function()
	{
		duration -= 1000;
		document.querySelectorAll('.time').forEach( e => e.innerText = (duration / 1000) + 's' );

		if( duration <= 0 )
		{
			clearInterval(interval);
			return false;
		}

	}, 1000 );

});

socket.on('spawn entity', function (msg)
{
	drawItem(msg, 'wait', 'spawn');
});

socket.on('despawn entity', function (msg)
{
	let item = document.getElementById(msg.uid).remove();
});

socket.on('stop entity', function (msg) {
	setTimeout(function()
	{
		drawItem(msg, 'wait');
	}, 250); // must be at speed time
});

socket.on('attack entity', function (msg) {
	setTimeout(function()
	{
		// reset mode to wait after one attack
		setTimeout(function()
		{
			drawItem(msg, 'wait');
		}, 950);

		drawItem(msg, 'attack');
	}, 0); // must be at speed time
});

socket.on('cast', function (msg)
{
	drawItem(msg, 'cast');
});

socket.on('hit entity', function (msg)
{
	showDamage(msg);
});

socket.on('heal entity', function (msg)
{
	showDamage(msg);
});

socket.on('miss entity', function (msg)
{
	msg.dmg = 'miss';
	showDamage(msg);
});

socket.on('skill use', function (msg)
{
	runEffect(msg[0], msg[1]);

	if( msg[2] )
		drawItem(msg[2], 'use');
});

socket.on('dead entity', function (msg)
{
	drawItem(msg, 'dead');
	setTimeout(function()
	{
		let element = document.getElementById(msg.uid);
		if( element ) element.remove();
	}, 2000);
});

socket.on('move entity', function (msg)
{
	drawItem(msg, 'walk');
});

function highlightArea(position)
{
	let area = document.getElementById("highlighter");
	area.style.top = parseInt(position[0] * tilesize) + "px";
	area.style.left = parseInt(position[1] * tilesize) + "px";
	area.style.width = tilesize + "px";
	area.style.height = tilesize + "px";
}

function targetArea(position)
{
	let target = document.getElementById("targeter");
	target.style.top = parseInt(position[0] * tilesize) + "px";
	target.style.left = parseInt(position[1] * tilesize) + "px";
	target.style.width = tilesize + "px";
	target.style.height = tilesize + "px";
}

let audiotimer = {};

for( let skillid in Types.SKILLS )
{
	let skill = Types.SKILLS[skillid];

	let duration = skill.tileset ? skill.tileset[2] : 3000;

	let template = '<div id="" class="effect" data-name="' + skillid + '" data-duration="' + duration + '">' +
		'<sprite class=""></sprite>' +
		'<audio controls preload="auto" data-duration="' + skill.audio[2] + '" data-start="' + skill.audio[1] + '"><source src="/audio/' + skill.audio[0] + '" /></audio>' +
		'</div>';

	document.querySelector(".skills").innerHTML += template;
}

/*debuging*/
setInterval(function()
{
	//runEffect('thunder', [10,10]);
}, 5000);

function runEffect(effectname, position)
{
	let effect = document.querySelector('.effect[data-name="' + effectname + '"]').cloneNode(true);

	document.querySelector('.objects').appendChild(effect);

	effect.style.left = parseInt(position[1] * tilesize) + "px";
	effect.style.top = parseInt(position[0] * tilesize) + "px";
	effect.style.display = 'block';

	effect.querySelector('sprite').className = 'run'; // start animation

	playAudio( effect.querySelector('audio') );

	setTimeout(function()
	{
		effect.remove();
	}, 3000 );
}

function drawItem(msg, mode, type)
{
	if( !msg.uid )
		return false;

	let direction = msg.d;
	let position = msg.p;
	let item = document.getElementById(msg.uid);

	let playerid = parseInt(msg.pl) + 1;

	let entity = Types.CHARS['king3'];
	if( playerid == 2 )
		entity = Types.CHARS['king2'];

	if( msg.t == 'char' )
	{
		entity = Types.CHARS[msg.n];
	}

	if( msg.t == 'monster' )
	{
		entity = Types.MOBS[msg.n];
	}

	if( !item )
	{
		let template = '<div id="' + msg.uid + '" data-playerid="' + playerid + '" class="' + msg.t + '" data-id="' + msg.id + '" data-name="' + msg.n + '">\n' +
			'\t\t\t\t<sprite style="background-image: url(' + entity.tileset + '); margin: -' + getRandomInt(3,7) + 'px 0 0 -' + getRandomInt(2,7) + 'px"></sprite>\n' +
			'\t\t\t\t<div class="cast">\n' +
			'\t\t\t\t\t<div class="bar"></div>\n' +
			'\t\t\t\t</div>\n' +
			'\t\t\t\t<div class="life">\n' +
			'\t\t\t\t\t<div class="bar"></div>\n' +
			'\t\t\t\t</div><div class="area"></div>\n' +
			'\t\t\t\t<div class="dmg template"></div>\n' +
			'\t\t\t\t[AUDIO]\n' +
			'\t\t\t\t[UPGRADES]\n' +
			'\t\t\t</div>';

		// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ //
		// audio
		// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ //
		let audio = '';
		for( let audiotype in entity.audio )
		{
			audio += '<audio controls preload="auto" class="' + audiotype + '" data-start="' + parseFloat( entity.audio[audiotype][1] ) + '" data-duration="' + parseFloat( entity.audio[audiotype][2] ) + '"><source src="/audio/' + entity.audio[audiotype][0] + '" /></audio>';
		}

		if( playerid == myPlayer.playerid )
			template = template.replace('[AUDIO]', audio);
		else
			template = template.replace('[AUDIO]', '');

		// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ //
		// upgrades
		// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ //
		let upgrades = '';

		if( entity.upgrades )
			for( let i in entity.upgrades )
			{
				let upgradeid = entity.upgrades[i];
				let upgrade = Types.CHARS[upgradeid];

				upgrades += '<div class="upgrade" data-entity="' + upgrade.name + '"><span class="cost">('+ upgrade.cost +'g)</span>' + upgrade.name + '</div>';
			}

		if( playerid == myPlayer.playerid )
			template = template.replace('[UPGRADES]', '<div class="upgrades">' + upgrades + '</div>');
		else
			template = template.replace('[UPGRADES]', '');

		let objects = document.querySelector(".objects");
		objects.innerHTML += template;
		item = document.getElementById(msg.uid);

		// upgrades
		if( msg.t == 'char' && playerid == myPlayer.playerid )
		{
			let area_defender = document.querySelector('.defender_area[data-c="' + position[0] + ',' + position[1] + '"]');
			area_defender.innerHTML = item.querySelector('.upgrades').cloneNode(true).outerHTML;

			bindAreas();
		}
	}

	item.style.transitionDuration = msg.s + 'ms';

	if( mode == 'wait' || mode == 'walk' )
	{
		item.style.left = parseInt(position[1] * tilesize) + "px";
		item.style.top = parseInt(position[0] * tilesize) + "px";
	}
	else
	{
		item.style.zIndex = ( position[0] * 32 ) + ( position[1] );
	}

	item.style.width = tilesize + "px";
	item.style.height = tilesize + "px";

	// life
	item.querySelector('.life .bar').style.width = parseInt( (msg.l / msg.m )  * 100) + "%";

	// castbar
	if( mode == 'cast' )
	{
		item.querySelector('.cast .bar').style.transitionDuration = msg.casttime + "ms";
		item.querySelector('.cast .bar').style.width = '100%';
	}

	if( mode == 'use' )
	{
		item.querySelector('.cast .bar').style.transitionDuration = 0;
		item.querySelector('.cast .bar').style.width = "0%";
	}

	// animation
	item.querySelector('sprite').className = 'char-' + mode + '-' + direction;
	item.querySelector('sprite').textContent = position[0] + ', ' + position[1];

	let area = item.querySelector('.area');
	area.style.left = parseInt(position[1] * tilesize) + "px";
	area.style.top = parseInt(position[0] * tilesize) + "px";
	area.style.width = tilesize + "px";
	area.style.height = tilesize + "px";

	let audio_voice = item.querySelector('audio.voice');
	let audio_hit = item.querySelector('audio.hit');
	let audio_dead = item.querySelector('audio.dead');
	let audio_walk = item.querySelector('audio.walk');

	if( type == 'spawn' && audio_voice )
	{
		playAudio(audio_voice, msg.uid);
	}

	if( mode == 'attack' && audio_hit )
	{
		playAudio(audio_hit, msg.uid);
	}

	if( mode == 'walk'  && audio_walk )
	{
		playAudio(audio_walk, msg.uid);
	}

	if( mode == 'dead' && audio_dead  )
	{
		playAudio(audio_dead, msg.uid);
	}

	/* camera
	if( msg.uid == 1 )
	{
		let camera = document.getElementById('camera');
		let camera_height = camera.clientHeight;
		let camera_width = camera.clientWidth;

		let diff = camera_height - (position[0] * tilesize);

		console.log(camera_height);
		console.log(camera_width);

		let camera_left = ( parseInt(position[1] * tilesize) - ( camera_width / 2 ) ),
			camera_top = ( parseInt(position[0] * tilesize) - ( camera_height / 2 ) );

		console.log(camera_left);

		let inner = document.getElementById('map');
		if( camera_top > ( inner.clientHeight - camera_height ) )
			camera_top = inner.clientHeight - camera_height;
		if( camera_top < 0 )
			camera_top = 0;
		if( camera_left > ( inner.clientWidth - camera_width ) )
			camera_left = inner.clientWidth - camera_width;
		if( camera_left < 0 )
			camera_left = 0;

		//if( camera_left > 0 || camera_left < ( camera_width / 2 )  )
		//	camera_left = 0;
		//if( camera_top > 0 || camera_top < ( camera_width / 2 ) )
		//	camera_top = 0;

		camera_left = camera_left * -1;
		camera_top = camera_top * -1;


		inner.style.left = camera_left + "px";
		inner.style.top = camera_top + "px";
	}
	*/
}

function playAudio( audio )
{
	let cln = audio.cloneNode(true);

	cln.currentTime = audio.dataset.start;
	cln.volume = 0.2;

	playPromise = cln.play();

	if (playPromise) {
		playPromise.then(() => {
			// Audio Loading Successful
			// Audio playback takes time
			setTimeout(() => {

				cln.pause();
				cln.currentTime = 0;

			}, parseInt(cln.dataset.duration)); // audio.duration is the length of the audio in seconds.

		}).catch((e) => {
			// Audio loading failure
		})
	}

}

function drawMap( map_json )
{
	console.log("draw map");

	let canvas = document.getElementById("map_canvas"),
		ctx = canvas.getContext("2d");

	let tilemap_size = 16;
	canvas.height = tilemap_size * map_json.height;
	canvas.width = tilemap_size * map_json.width;

	let tileset = new Image();
	tileset.src = '/map_bg_extended.png';
	tileset.onload = function ()
	{
		// tileset breite und höhe einbeziehen
		let tileset_x = Math.round(tileset.height / tilemap_size);
		let tileset_y = Math.round(tileset.width / tilemap_size);

		for( let i in map_json.layers )
		{
			let layer = map_json.layers[i];
			let layerid = layer.id;

			let zone = null;
			if( layer.properties && layer.properties.filter(obj => {return obj.name == 'zone'})[0] )
				zone = layer.properties.filter(obj => {return obj.name == 'zone'})[0].value;

			let playerNr = null;
			if( layer.properties && layer.properties.filter(obj => {return obj.name == 'player'})[0] )
				playerNr = parseInt( layer.properties.filter(obj => {return obj.name == 'player'})[0].value );

			//console.log( "player nr: " + playerNr );
			//console.log( "zone: " + zone );

			// draw
			if( layer.visible == true && layer.data )
			{
				for( let j in layer.data )
				{
					if( layer.data[j] == 0 )
						continue;

					let tileid = layer.data[j] - 1;

					// locate postion on png tileset
					let tile_on_tileset_y = Math.floor(tileid / tileset_y);
					let tile_on_tileset_x = Math.floor(tileid - ( tileset_y * tile_on_tileset_y ) );

					// target tile on canvas (32x32)
					let target_on_map_y = Math.floor(j / map_json.width);
					let target_on_map_x = Math.floor(j - ( map_json.height * target_on_map_y ) );

					// Create a pattern, offscreen
					const patternCanvas = document.createElement('canvas');
					const patternContext = patternCanvas.getContext('2d');

					// Give the pattern a width and height of 32
					patternCanvas.width = tilemap_size;
					patternCanvas.height = tilemap_size;

					// get tile from tileset map (png)
					patternContext.drawImage(tileset, tile_on_tileset_x * tilemap_size, tile_on_tileset_y * tilemap_size, tilemap_size, tilemap_size, 0, 0, tilemap_size, tilemap_size);

					//const pattern = ctx.createPattern(patternCanvas, 'repeat');
					//ctx.fillStyle = pattern;
					//ctx.fillRect(y, x, tilesize, tilesize);
					ctx.drawImage(patternCanvas, 0, 0, tilemap_size, tilemap_size, target_on_map_x * tilemap_size, target_on_map_y * tilemap_size, tilemap_size, tilemap_size);
				}
			}

			// own defender areas
			if( zone == 'defender' && myPlayer.playerid == playerNr )
			{
				document.querySelectorAll('.defender_area').forEach(e => e.remove());

				for( let j in layer.objects )
				{
					let object = layer.objects[j];

					// target tile on canvas (32x32)
					let target_on_map_y = Math.floor(object.y / object.width );
					let target_on_map_x = Math.floor(object.x / object.height );

					//let pos = coordQuad([target_on_map_y, target_on_map_x]);
					let pos = [target_on_map_y, target_on_map_x];

					let item = document.createElement('div');
					item.style.width = (tilesize * 1) + "px" ;
					item.style.height = (tilesize * 1) + "px" ;
					item.style.left = ( pos[1] * tilesize ) + "px";
					item.style.top = ( pos[0] * tilesize ) + "px";
					item.dataset.c =  pos[0] + ',' +  pos[1];
					item.className = 'defender_area';
					item.innerHTML = '<div class="upgrades"><div class="upgrade" data-entity="novice"><span class="cost">(100g)</span>novice</div></div>';
					map.insertBefore(item, map.firstChild);
				}

				bindAreas();
			}
		}
	};

	//map.style.width = (map_size[0] * tilesize) + "px";
	//map.style.height = (map_size[1] * tilesize) + "px";
}

function bindAreas()
{
	let areas = document.querySelectorAll(".defender_area");
	[].forEach.call(areas, function(area)
	{
		delete area.onclick;

		area.addEventListener("click", function(event)
		{
			event.preventDefault();

			areas.forEach( e => e.classList.remove("show-upgrades") );
			this.classList.add("show-upgrades");

			return false;
		});

		let upgrades = area.querySelectorAll('.upgrade');

		[].forEach.call(upgrades, function(upgrade)
		{
			delete upgrade.onclick;

			upgrade.addEventListener("click", function(event)
			{
				event.preventDefault();

				let pos = area.dataset.c.split(',');
				buyDefender( upgrade.dataset.entity, [pos[0],pos[1]] );

				return false;
			});
		});
	});

}

function buyDefender(entity, pos)
{
	if( entity == 'novice' )
		socket.emit('buy defender', {entity: entity, pos: pos});
	else
		socket.emit("upgrade defender", {entity: entity, pos: pos });
}

let timer_move = Date.now();


let sound_started = false;
function makeSound(element)
{
	if( sound_started )
		return false;

	let lines = [
		[22.0, 1500],
		[29.2, 1000],
		[34.0, 1500],
		[2.5, 1000],
		[1.0, 1000],
	];

	let line = lines[getRandomInt(0, lines.length - 1)];

	//console.log("play sound time: " + line);
	//console.log(lines);

	// random voice wäre geil
	element.currentTime = line[0];
	element.play();
	sound_started = true;
	setTimeout(function()
	{
		element.pause();
		sound_started = false;
	}, line[1]);
}

document.getElementById("map_canvas").addEventListener("mousemove", function(event)
{
	let x = Math.floor( event.offsetX / tilesize);
	let y = Math.floor( event.offsetY / tilesize);

	highlightArea([y, x]);
});

showHeal = function(msg)
{
	let element = document.getElementById(msg.uid);
	let dmgbox = element.querySelector('.dmg');
	//dmgbox.style.left = parseInt(msg.p[1] * tilesize) + "px";
	//dmgbox.style.top = parseInt(msg.p[0] * tilesize) + "px";
	dmgbox.style.marginLeft += (getRandomInt(0, 3) * -1) + 'px';
	dmgbox.style.marginTop += (getRandomInt(0, 3) * -1) + 'px';
	dmgbox.innerText = msg.dmg;
	dmgbox.className = "dmg show heal";

	setTimeout(function()
	{
		dmgbox.className = "dmg";
	}, 1000);
};

showDamage = function(msg)
{
	let element = document.getElementById(msg.uid);
	let template = element.querySelector('.dmg.template');

	let dmgbox = template.cloneNode(true);
	//dmgbox.style.left = parseInt(msg.p[1] * tilesize) + "px";
	//dmgbox.style.top = parseInt(msg.p[0] * tilesize) + "px";
	dmgbox.style.marginLeft += (getRandomInt(0, 3) * -1) + 'px';
	dmgbox.style.marginTop += (getRandomInt(0, 32) * -1) + 'px';
	dmgbox.innerText = msg.dmg;
	dmgbox.className = "dmg show";

	template.parentNode.insertBefore(dmgbox, template);

	setTimeout(function()
	{
		dmgbox.remove();
	}, 1000);

};

function getRandomInt(min, max) {
	min = Math.ceil(min);
	max = Math.floor(max);
	return Math.floor(Math.random() * (max - min)) + min;
}

function coordQuad(coord, factor)
{
	if( !factor )
		factor = 2;

	let y = Math.floor(coord[0] / factor);
	let x = Math.floor(coord[1] / factor);

	//console.log("quadration");
	//console.log(coord);
	//console.log([y,x]);

	return [y,x];
};

function getCookie(cname)
{
	let name = cname + "=";
	let decodedCookie = decodeURIComponent(document.cookie);
	let ca = decodedCookie.split(';');
	for(let i = 0; i <ca.length; i++) {
		let c = ca[i];
		while (c.charAt(0) == ' ') {
			c = c.substring(1);
		}
		if (c.indexOf(name) == 0) {
			return c.substring(name.length, c.length);
		}
	}
	return "";
}

document.querySelectorAll('#game_start h1').forEach(e =>
{
	e.addEventListener("mouseover", function()
	{
		let spore = document.getElementById("spore");
		spore.className = spore.className.replace('wait', 'dead');
	});

	e.addEventListener("mouseout", function()
	{
		let spore = document.getElementById("spore");
		spore.className = spore.className.replace('dead', 'wait');
	})
});

document.querySelectorAll('#login').forEach(e =>
{
	e.addEventListener("mouseover", function()
	{
		let spore = document.getElementById("orchero");
		spore.className = spore.className.replace('wait-s', 'attack-n');
	});

	e.addEventListener("mouseout", function()
	{
		let spore = document.getElementById("orchero");
		spore.className = spore.className.replace('attack-n', 'wait-s');
	})
});

document.querySelectorAll('#login input').forEach(e =>
{
	e.addEventListener("focus", function()
	{
		let spore = document.getElementById("mummy");
		spore.className = spore.className.replace('wait-so', 'dead-o');
	});

	e.addEventListener("blur", function()
	{
		let spore = document.getElementById("mummy");
		spore.className = spore.className.replace('dead-o', 'wait-so');
	})
});