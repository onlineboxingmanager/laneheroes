let Types = require('./gametypes');

Waves =
	[
		[], // leer

		[
			[1, Types.MOBS.spore],
			[3, Types.MOBS.spore],
			[6, Types.MOBS.spore],
		],

		[
			[4, Types.MOBS.spore],
			[9, Types.MOBS.spore],
			[6, Types.MOBS.spore],
			[3, Types.MOBS.spider],
		],

		[
			[4, Types.MOBS.spore],
			[9, Types.MOBS.spore],
			[6, Types.MOBS.spore],
			[3, Types.MOBS.spider],
		],

		[
			[1, Types.MOBS.spider],
			[2, Types.MOBS.spider],

			[4, Types.MOBS.spore],
			[6, Types.MOBS.spore],
			[9, Types.MOBS.spore],
			[11, Types.MOBS.spore],
		],

		[
			[1, Types.MOBS.crab],
			[3, Types.MOBS.crab],
			[6, Types.MOBS.crab],
		],

		[
			[0, Types.MOBS.crab],
			[2, Types.MOBS.spider],
			[5, Types.MOBS.spider],
			[7, Types.MOBS.wolf],
			[10, Types.MOBS.wolf],
		],

		// versus
		[],

		[
			[2, Types.MOBS.orc],
			[5, Types.MOBS.orc],
			[0, Types.MOBS.orc],
		],

		[
			[0, Types.MOBS.orc],
			[2, Types.MOBS.orc],
			[5, Types.MOBS.orc],
			[7, Types.MOBS.wolf],
			[9, Types.MOBS.wolf],
		],

		// versus
		[],

		[
			[5, Types.MOBS.boss_orchero],
		],

		[
			[0, Types.MOBS.mummy],
			[5, Types.MOBS.mummy],
			[2, Types.MOBS.mummy],
		],

		[
			[0, Types.MOBS.mummy],
			[2, Types.MOBS.mummy],
			[5, Types.MOBS.mummy],
			[7, Types.MOBS.mummy],
			[10, Types.MOBS.mummy],
		],

		// versus
		[],

		[
			[5, Types.MOBS.boss_chimera],
		],

		[
			[5, Types.MOBS.icewolf],
			[2, Types.MOBS.icewolf],
		],

		[
			[2, Types.MOBS.icewolf],
			[5, Types.MOBS.icewolf],
			[7, Types.MOBS.icewolf],
		],

	];

if(!(typeof exports === 'undefined')) {
	module.exports = Waves;
}