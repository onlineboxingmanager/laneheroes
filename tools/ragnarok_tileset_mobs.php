<?php
#�

$base_path = __DIR__ . '/../sprites/mobs/';

$config = array();

$tile = 240;

$tileset_width = $tile * 8;
$tileset_height = $tile * 8;

$css_mob_base = '';

foreach (glob($base_path . "/*", GLOB_ONLYDIR) as $folder )
{

	$monstername = basename($folder);
	var_dump($monstername);

	$full_tileset = imagecreatetruecolor( $tileset_width, $tileset_height );

	$full_tileset_color = imagecolorallocatealpha($full_tileset, 0, 0, 0, 127);
	imagefill($full_tileset,0,0, $full_tileset_color);

	imagealphablending($full_tileset, false);
	imagesavealpha($full_tileset, true);

	$filled = [];

	$files = glob( $folder . '/*.png' );

	foreach( $files as $i => $file )
	{
		$y = floor( $i / 8 );
		$x = $i - ( $y * 8 );

		$size = filesize($file);
		$is_empty = ( $size <= 2048 );

		$filled[$y][$x] = $size;

		if( $is_empty )
			continue;

		$image = imagecreatetruecolor( $tile, $tile );

		$transparent = imagecolorallocatealpha($image, 0, 0, 0, 127);
		imagefill($image, 0, 0, $transparent);

		// turning off alpha blending (to ensure alpha channel information
		// is preserved, rather than removed (blending with the rest of the
		// image in the form of black))
		imagealphablending($image, false);

		// turning on alpha channel information saving (to ensure the full range
		// of transparency is preserved)
		imagesavealpha($image, true);

		// zentriert sprite -> tile
		$sprite = imagecreatefrompng($file);
		imagealphablending($sprite, false); // setting alpha blending on
		imagesavealpha($sprite, true); // save alphablending setting (important)

		$sprite_width = imagesx($sprite);
		$sprite_height = imagesy($sprite);

		$center_y = round(($tile - $sprite_height) / 2);
		$center_x = round(($tile - $sprite_width) / 2);
		imagecopyresampled($image, $sprite, $center_x, $center_y, 0, 0, $sprite_width, $sprite_height, $sprite_width, $sprite_height);

		// copy direction tileset to full tileset
		imagecopyresampled($full_tileset, $image, $x * $tile, $y * $tile, 0, 0, $tileset_width, $tileset_height, $tileset_width, $tileset_height);

		imagedestroy($image);
		imagedestroy($sprite);
	}

	imagepng($full_tileset, $base_path . 'tileset_'.$monstername.'_240x240.png' );
	imagedestroy($full_tileset);


	$actions =
		[
			'wait',
			'wait',
			'walk',
			'walk',
			'attack',
			'attack',
			'dead',
			'dead'
		];

	$css_prefix = '.monster[data-name="[MONSTER]"] ';
	$css_template_s = '
[PREFIX].char-xxx-s,
[PREFIX].char-xxx-sw,
[PREFIX].char-xxx-so,
[PREFIX].char-xxx-w
{
	animation-name: mob_[MONSTER]_xxx_s;
}

@keyframes mob_[MONSTER]_xxx_s
{
[KEYFRAMES]
}
';

	$css_template_n = '
[PREFIX].char-xxx-n,
[PREFIX].char-xxx-nw,
[PREFIX].char-xxx-no,
[PREFIX].char-xxx-o
{
	animation-name: mob_[MONSTER]_xxx_n;
}

@keyframes mob_[MONSTER]_xxx_n
{
[KEYFRAMES]
}
';

	$animation_template = "\t" . '[PERCENT]% ' . "\t" . '{ background-position: calc(((240px * (8 * 0)) + (240px * [FRAME])) * -1 )   	calc(240px * -[Y])	}';

	$css = '
.monster[data-name="[MONSTER]"] sprite
{
	background-image: url(\'/mobs/tileset_'.$monstername.'_240x240.png\');
}'."\n\n";

	$css .= '
.monster[data-name="[MONSTER]"] .char-dead-s,
.monster[data-name="[MONSTER]"] .char-dead-o,
.monster[data-name="[MONSTER]"] .char-dead-w,
.monster[data-name="[MONSTER]"] .char-dead-n,
.monster[data-name="[MONSTER]"] .char-dead-sw,
.monster[data-name="[MONSTER]"] .char-dead-so,
.monster[data-name="[MONSTER]"] .char-dead-no,
.monster[data-name="[MONSTER]"] .char-dead-nw
{
	animation-duration: 1000ms;
	animation-iteration-count: 1;
	animation-timing-function: step-start;
	animation-fill-mode: forwards;
}
'."\n\n";

	foreach( $filled as $y => $list )
	{
		$actionname = $actions[$y];

		$line = str_replace('xxx', $actionname, $css_template_n);
		if( $y % 2 == 0 )
			$line = str_replace('xxx', $actionname, $css_template_s);

		$frames = 0;
		foreach( $list as $x => $size )
		{
			if( $size > 2048 )
				$frames++;
		}

		$framelist = [];
		for ( $frame = 0; $frame < $frames; $frame++ )
		{
			$percent = round(( 100 / $frames ) * $frame, 2);

			$animation_line = str_replace('[PERCENT]', $percent, $animation_template);
			$animation_line = str_replace('[FRAME]', $frame, $animation_line);
			$animation_line = str_replace('[Y]', $y, $animation_line);

			$framelist[] = $animation_line;
		}

		// add 100%
		$animation_line = str_replace('[PERCENT]', 100, $animation_template);
		$animation_line = str_replace('[FRAME]', ( $actionname == 'dead' ? $frame-1 : 0 ), $animation_line);
		$animation_line = str_replace('[Y]', $y, $animation_line);
		$framelist[] = $animation_line;

		$line = str_replace('[KEYFRAMES]', implode("\n", $framelist), $line);
		$css .= $line;
	}

	$css = str_replace('[PREFIX]', $css_prefix, $css);
	$css = str_replace('[MONSTER]', $monstername, $css);

	file_put_contents($base_path . $monstername . '.css', $css);

	$css_mob_base .= '@import url("/mobs/'.$monstername.'.css");'."\n";
}

file_put_contents($base_path . 'mobs.css', $css_mob_base);