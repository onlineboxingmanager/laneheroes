let BaseClass = require('./lib/class');

class Message extends BaseClass
{

	constructor()
	{
		super();
	};

	static send(action, value)
	{

	};

	static spawn(entity)
	{
		console.log("spawn " + entity.position.toString());
		entity.game.sendSockets('spawn entity', entity.getState()); // remove me - replaced by server ticks
	};

	static despawn(entity)
	{
		console.log("despawn " + entity.position.toString());
		entity.game.sendSockets('despawn entity', entity.getState()); // remove me - replaced by server ticks
	};
}

module.exports = Message;