<?php
#�
die("alt");
$base_path = __DIR__ . '/../sprites';

$config = array();
$config['gender'] = array(0, 1);
$config['direction'] = array(0, 1, 2, 3, 4, 5, 6, 7);
$config['action'] = array
(
	0 => array( 0 ), // stehen
	1 => array( 0, 1, 2, 3, 4, 5, 6, 7 ), // laufen
	2 => array( 0 ), // sitzen
	3 => array( 0, 1, 2 ), // aufheben
	4 => array( 0, 1, 2, 3, 4, 5 ), // kampfhaltung 1
	5 => array( 0, 1, 2, 3, 4 ), // angriff
	6 => array( 0, 1, 2 ), // selbst getroffen
	7 => array( 0 ), // sterben
	8 => array( 0 ), // tot
	9 => array( 0 ), // alt attack 1
	10 => array( 0, 1, 2, 3, 4, 5, 6, 7, 8 ), // alt attack 2
	11 => array( 0, 1, 2, 3, 4, 5, 6, 7 ), // alt attack 3
	12 => array( 0, 1, 2, 3, 4, 5 ), // zaubern
);
$config['job'] = array(
	0, // newbie
	1, // sword
	2, // mage
	3, // archer
	4, // acolyte
	5, // merch
	6, // thief
	
	7, // knight
	8, // priest
	9, // wiz
	10, // blacksmith
	11, // hunter
	12, // assa

	14, // paladin
	15, // monk
	16, // sage
	17, // rouge
	18, // ?? alchemist ?
	//19, // dancer ?
	20, // dancer
	
	// special
	13, // mounted knigth
	21, // mounted pala
);

// debug
$config['direction'] = array(0, 1, 2, 3, 4);
$config['job'] = array(0, 1, 2, 3);


$tile = 240;

$tileset_width = $tile * 9;
$tileset_height = $tile * count($config['action']);

foreach( $config['gender'] as $gender )
{
	foreach( $config['job'] as $job )
	{
		
		$full_tileset = imagecreatetruecolor( $tileset_width * 5, $tileset_height * 5 );
		imagesavealpha($full_tileset, true);
		
		$full_tileset_color = imagecolorallocatealpha($full_tileset, 0, 0, 0, 127);
		imagefill($full_tileset, 0, 0, $full_tileset_color);
		
		$i = 0;
		foreach( $config['direction'] as $direction )
		{
			$hair = 1;
			$dir = $base_path . '/' . $gender . '/' . $job . '/' . $hair;
			
			$image = imagecreatetruecolor( $tileset_width, $tileset_height );
			imagesavealpha($image, true);
			//imagealphablending($image, false);
			
			$color = imagecolorallocatealpha($image, 0, 0, 0, 127);
			imagefill($image, 0, 0, $color);
			
			//$trans_color = imagecolorallocatealpha($image, 255, 0, 255, 0); // pink
			//imagefill($image, 0, 0, $trans_color);
		
			foreach( $config['action'] as $action_key => $framelist )
			{
				foreach( $framelist as $frame )
				{
					
					$file = $dir . '/direction_'.$direction.'__action_'.$action_key.'__frame_'.$frame.'.png';
					
					$sprite = imagecreatefrompng($file);
					//imagesavealpha($sprite, true);
					
					// save transparency
					#imagealphablending($sprite, false);
					#imagesavealpha($sprite, false);
									
					#$pink_index = imagecolorallocatealpha($sprite, 255, 0, 255, 127); // pink
					#$pink_index = imagecolorallocate($sprite, 255, 0, 255);
					#imagecolortransparent($sprite, $pink_index);
					
					
					#die();
					
					#imagecopyresampled($image, $sprite, $tile * $frame, $tile * $action_key, 0, 0, $tile, $tile, $tile, $tile);
					imagecopy($image, $sprite, $tile * $frame, $tile * $action_key, 0, 0, $tile, $tile);
					
				}
			}
			
			$tileset = $dir . '/tileset_' . $direction . '.png';
			imagepng($image, $tileset);
			
			// copy direction tileset to full tileset
			imagecopy($full_tileset, $image, 0, $i * $tileset_height, 0, 0, $tileset_width, $tileset_height);
			
			imagedestroy($image);

			
			$i++;
		}

		imagepng($full_tileset, $base_path . '/' . $gender . '/' . $job . '/tileset.png' );
		imagedestroy($full_tileset);
	}
}